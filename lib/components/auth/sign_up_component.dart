import 'dart:convert';

import 'package:adekuflutter/components/stateLess/BackgroundState.dart';
import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/CustomWebView.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/constants/images_constant.dart';

class SignUpComponent extends StatefulWidget {
  @override
  State createState() => SignUpComponentState();
}

class SignUpComponentState extends State<SignUpComponent> {
  var width, height;
  bool isVisible = true, isDoRegistration = false, isReadTermsCondition = false;
  TextEditingController fullNameController,
      phoneNumberController,
      emailController,
      passwordController;

  @override
  void initState() {
    super.initState();
    fullNameController = TextEditingController();
    phoneNumberController = TextEditingController();
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  void doSignUp() async {
    var data = Map<String, dynamic>();
    data[KeyParams.fullName] = fullNameController.text;
    data[KeyParams.phone] = "0${phoneNumberController.text}";
    data[KeyParams.email] = emailController.text;
    data[KeyParams.password] = passwordController.text;
    await formURLEncoded(Req.registerNew, data).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      setState(() {
        isDoRegistration = false;
      });
      if (data["success"]) {
        setAuth(data["result"]);
        openCheckVerify(data["result"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        isDoRegistration = false;
      });
      alertError(context, onError.toString());
      print(onError.toString());
    });
  }

  Future openCheckVerify(dynamic data) async {
    var verify = await Navigator.of(context).push(new MaterialPageRoute<bool>(
        builder: (BuildContext context) {
          return new CustomWebView(
            state: CustomWebViewAction.verification,
            url: "",
            content: data,
          );
        },
        fullscreenDialog: true));
    if (verify != null) {
      if (verify) {
        gotoHome();
      } else {
        setState(() {
          isDoRegistration = false;
        });
        alertError(context, "Gagal melakukan verifikasi");
      }
    } else {
      setState(() {
        isDoRegistration = false;
      });
      alertError(context, "Gagal melakukan verifikasi");
    }
  }

  void loadTermsAndCondition() async {
    var data = Map<String, dynamic>();
    await formURLEncoded(Req.registerTerms, data).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["tos"]["success"]) {
        setState(() {
          isReadTermsCondition = true;
        });
      }
    }).catchError((onError) {
      print(onError.toString());
    });
  }

  Future openTermsAndCondition() async {
    var isAccept = await Navigator.of(context).push(new MaterialPageRoute<bool>(
        builder: (BuildContext context) {
          return new CustomWebView(
              state: CustomWebViewAction.termCondition,
              url:
                  "https://www.termsfeed.com/blog/terms-service-url-facebook-app/");
        },
        fullscreenDialog: true));
    if (isAccept != null) {
      setState(() {
        isReadTermsCondition = isAccept;
      });
    }
  }

  void gotoHome() {
    Application.router.navigateTo(context, Routes.home,
        clearStack: true, replace: true, transition: TransitionType.fadeIn);
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.primary,
        body: BackgroundState(
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              title: Text(Values.titleRegistration,
                  textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 32),
                Image.asset(Drawable.text_logo, width: 165),
                SizedBox(height: 16),
                formRegistration(),
                termsAndCondition(),
                buttonRegistration()
              ],
            )));
  }

  Widget formRegistration() {
    return BoxBrownState(
        width: width,
        child: Container(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Column(children: [
              SizedBox(height: 16),
              Text(Values.titleRegistration,
                  style: TextStyle(fontSize: 20, color: ColorStyle.white),
                  textAlign: TextAlign.center),
              SizedBox(height: 16),
              TextState(
                  hint: Values.labelFullName,
                  controller: fullNameController,
                  onChange: () {}),
              SizedBox(height: 8),
              Row(
                children: [
                  Container(
                      child: Text("+62",
                          style:
                              TextStyle(color: ColorStyle.white, fontSize: 16)),
                      padding: EdgeInsets.only(
                          left: 16, right: 16, top: 20, bottom: 20),
                      margin: EdgeInsets.only(right: 8),
                      decoration: BoxDecoration(
                          color: ColorStyle.brownLight,
                          borderRadius: BorderRadius.circular(8))),
                  Expanded(
                      child: PhoneNumberState(
                          controller: phoneNumberController,
                          onChange: () {
                            var phoneNumber = phoneNumberController.text;
                            if (phoneNumber.length >= 1) {
                              if (phoneNumber[0] == "0")
                                setState(() {
                                  phoneNumberController.text = "";
                                });
                            }
                          }))
                ],
              ),
              SizedBox(height: 8),
              EmailState(controller: emailController, onChange: () {}),
              SizedBox(height: 8),
              PasswordState(
                  controller: passwordController,
                  obsecure: isVisible,
                  onChange: () {
                    setState(() {
                      isVisible = !isVisible;
                    });
                  }),
              SizedBox(height: 16),
            ])));
  }

  Widget termsAndCondition() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Checkbox(
            value: isReadTermsCondition,
            onChanged: (bool value) {
              loadTermsAndCondition();
              // openTermsAndCondition();
            },
          ),
          Expanded(
              child: Text(
            Values.descriptionTermsAndCondition,
            style: TextStyle(color: ColorStyle.white),
          ))
        ],
      ),
    );
  }

  Widget buttonRegistration() {
    return FlatButton(
        onPressed: () {
          if (!isDoRegistration) {
            if (isReadTermsCondition) {
              setState(() {
                isDoRegistration = true;
              });
              doSignUp();
            } else {
              alertError(context, Values.descriptionTermsAndConditionAgree);
            }
          }
        },
        child: Container(
            width: width,
            margin: EdgeInsets.all(16),
            padding: EdgeInsets.all(16),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: ColorStyle.white,
                borderRadius: BorderRadius.all(Radius.circular(16))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(Values.buttonNewRegistration),
                (!isDoRegistration)
                    ? Container()
                    : SizedBox(
                        width: 16,
                      ),
                (!isDoRegistration)
                    ? Container()
                    : SizedBox(
                        width: 24,
                        height: 24,
                        child: CircularProgressIndicator())
              ],
            )));
  }
}
