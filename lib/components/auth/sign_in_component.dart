import 'dart:convert';

import 'package:adekuflutter/components/stateLess/BackgroundState.dart';
import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/CustomWebView.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/constants/images_constant.dart';

class SignInComponent extends StatefulWidget {
  @override
  State createState() => SignInComponentState();
}

class SignInComponentState extends State<SignInComponent> {
  var width, height;
  bool isVisible = true, isDoLogin = false;
  TextEditingController phoneNumberController, passwordController;
  dynamic resLogin;

  @override
  void initState() {
    super.initState();
    phoneNumberController = TextEditingController();
    passwordController = TextEditingController();
  }

  void doSignIn() async {
    var body = Map<String, dynamic>();
    body[KeyParams.phone] = "0${phoneNumberController.text}";
    body[KeyParams.password] = passwordController.text;
    await formData(Req.loginNew, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        setState(() {
          resLogin = data["results"];
        });
        openOTP();
      } else {
        setState(() {
          isDoLogin = false;
        });
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        isDoLogin = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void doUpdateOTP(String otp) async {
    var body = Map<String, dynamic>();
    body[KeyParams.phone] = "0${phoneNumberController.text}";
    body[KeyParams.otp] = otp;
    await formData(Req.loginOTP, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      setState(() {
        isDoLogin = false;
      });
      if (data["success"]) {
        setAuth(data["results"]);
        openCheckVerify(data["results"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        isDoLogin = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  Future openCheckVerify(dynamic data) async {
    var verify = await Navigator.of(context).push(new MaterialPageRoute<bool>(
        builder: (BuildContext context) {
          return new CustomWebView(
            state: CustomWebViewAction.verification,
            url: "",
            content: data,
          );
        },
        fullscreenDialog: true));
    if (verify != null) {
      if (verify) {
        gotoHome();
      } else {
        setState(() {
          isDoLogin = false;
        });
        alertError(context, "Gagal melakukan verifikasi");
      }
    } else {
      setState(() {
        isDoLogin = false;
      });
      alertError(context, "Gagal melakukan verifikasi");
    }
  }

  Future openOTP() async {
    var otp = await Navigator.of(context).push(new MaterialPageRoute<String>(
        builder: (BuildContext context) {
          return new CustomWebView(
              state: CustomWebViewAction.otp, url: "", content: resLogin);
        },
        fullscreenDialog: true));
    if (otp != null) {
      if (otp != "") {
        doUpdateOTP(otp);
      } else {
        setState(() {
          isDoLogin = false;
        });
        alertError(context, "Gagal melakukan otentikasi");
      }
    } else {
      setState(() {
        isDoLogin = false;
      });
      alertError(context, "Gagal melakukan otentikasi");
    }
  }

  void gotoHome() {
    Application.router.navigateTo(context, Routes.home,
        clearStack: true, replace: true, transition: TransitionType.fadeIn);
  }

  void gotoVerification() {
    Application.router.navigateTo(context, Routes.pin,
        clearStack: true, replace: true, transition: TransitionType.fadeIn);
  }

  void gotoSignUp() {
    Application.router.navigateTo(context, Routes.signUp,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  void gotoLostPassword() {
    Application.router.navigateTo(context, Routes.lostPassword,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.primary,
        body: BackgroundState(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(Drawable.text_logo, width: 125),
            Image.asset(Drawable.face_logo, width: 125, height: 134),
            formLogin(),
            lostPassword(),
            buttonLogin(),
            descriptionRegistration()
          ],
        )));
  }

  Widget formLogin() {
    return BoxBrownState(
      width: width,
      child: Container(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: Column(children: [
            SizedBox(height: 16),
            Text(Values.titleLogin,
                style: TextStyle(fontSize: 20, color: ColorStyle.white),
                textAlign: TextAlign.center),
            SizedBox(height: 16),
            Row(
              children: [
                Container(
                    child: Text("+62",
                        style:
                            TextStyle(color: ColorStyle.white, fontSize: 16)),
                    padding: EdgeInsets.only(
                        left: 16, right: 16, top: 20, bottom: 20),
                    margin: EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                        color: ColorStyle.brownLight,
                        borderRadius: BorderRadius.circular(8))),
                Expanded(
                    child: PhoneNumberState(
                        controller: phoneNumberController,
                        onChange: () {
                          var phoneNumber = phoneNumberController.text;
                          if (phoneNumber.length >= 1) {
                            if (phoneNumber[0] == "0")
                              setState(() {
                                phoneNumberController.text = "";
                              });
                          }
                        }))
              ],
            ),
            SizedBox(height: 8),
            PasswordState(
                controller: passwordController,
                obsecure: isVisible,
                onChange: () {
                  setState(() {
                    isVisible = !isVisible;
                  });
                }),
            SizedBox(height: 16),
          ])),
    );
  }

  Widget lostPassword() {
    return Container(
        margin: EdgeInsets.only(right: 16, bottom: 16),
        padding: EdgeInsets.only(top: 8),
        child: GestureDetector(
          onTap: () {
            gotoLostPassword();
          },
          child: Align(
              alignment: Alignment.bottomRight,
              child: Text(Values.labelLostPassword,
                  style: TextStyle(
                      color: ColorStyle.brown, fontStyle: FontStyle.italic))),
        ));
  }

  Widget buttonLogin() {
    return FlatButton(
        onPressed: () {
          if (!isDoLogin) {
            setState(() {
              isDoLogin = true;
            });
            doSignIn();
          }
        },
        child: Container(
            width: width,
            margin: EdgeInsets.all(16),
            padding: EdgeInsets.all(16),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: ColorStyle.white,
                borderRadius: BorderRadius.all(Radius.circular(16))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(Values.buttonLogin),
                (!isDoLogin)
                    ? Container()
                    : SizedBox(
                        width: 16,
                      ),
                (!isDoLogin)
                    ? Container()
                    : SizedBox(
                        width: 24,
                        height: 24,
                        child: CircularProgressIndicator())
              ],
            )));
  }

  Widget descriptionRegistration() {
    return Container(
        margin: EdgeInsets.all(16),
        child: GestureDetector(
          onTap: () {
            gotoSignUp();
          },
          child: RichText(
              text: TextSpan(
                  text: Values.descriptionNotHaveAccount,
                  style: TextStyle(color: ColorStyle.brown, fontSize: 14),
                  children: [
                TextSpan(
                    text: Values.buttonNewRegistration,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: ColorStyle.white)),
              ])),
        ));
  }
}
