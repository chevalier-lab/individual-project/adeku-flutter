import 'dart:convert';

import 'package:adekuflutter/components/stateLess/BackgroundState.dart';
import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/constants/images_constant.dart';

class LostPasswordComponent extends StatefulWidget {
  @override
  State createState() => LostPasswordComponentState();
}

class LostPasswordComponentState extends State<LostPasswordComponent> {
  var width, height;
  TextEditingController emailController;
  bool isDoForgotPassword = false;

  @override
  void initState() {
    super.initState();
    emailController = TextEditingController();
  }

  void doLostPassword() async {
    var body = Map<String, dynamic>();
    body[KeyParams.user] = emailController.text;
    await formData(Req.forgotPassword, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      setState(() {
        isDoForgotPassword = false;
      });
      if (data["success"]) {
        setState(() {
          emailController.clear();
        });
        alertSuccess(context,
            "Kami sudah mengirim link forgot password, pada email anda");
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        isDoForgotPassword = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.primary,
        body: BackgroundState(
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.transparent,
              title: Text(Values.titleLostPassword,
                  textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(Drawable.undraw_forgot_password_gi2d, width: 205),
                SizedBox(height: 16),
                BoxBrownState(
                    width: width,
                    child: Container(
                        padding: EdgeInsets.only(left: 16, right: 16),
                        child: Column(children: [
                          SizedBox(height: 16),
                          Text(Values.descriptionResetPassword,
                              style: TextStyle(
                                  fontSize: 14, color: ColorStyle.white),
                              textAlign: TextAlign.center),
                          SizedBox(height: 16),
                          EmailState(
                              controller: emailController, onChange: () => {}),
                          SizedBox(height: 16),
                        ]))),
                SizedBox(height: 16),
                buttonLostPassword()
              ],
            )));
  }

  Widget buttonLostPassword() {
    return FlatButton(
        onPressed: () {
          if (!isDoForgotPassword) {
            setState(() {
              isDoForgotPassword = true;
            });
            doLostPassword();
          }
        },
        child: Container(
          width: width,
          margin: EdgeInsets.all(16),
          padding: EdgeInsets.all(16),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: ColorStyle.white,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(Values.buttonSend),
              (!isDoForgotPassword)
                  ? Container()
                  : SizedBox(
                      width: 16,
                    ),
              (!isDoForgotPassword)
                  ? Container()
                  : SizedBox(
                      width: 24, height: 24, child: CircularProgressIndicator())
            ],
          ),
        ));
  }
}
