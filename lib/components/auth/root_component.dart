import 'package:adekuflutter/components/stateLess/BackgroundState.dart';
import 'package:adekuflutter/components/stateLess/CustomWebView.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:gson/gson.dart';

class RootComponent extends StatefulWidget {
  @override
  State createState() => RootComponentState();
}

class RootComponentState extends State<RootComponent> {
  void setDelay() {
    // Delayed
    Future.delayed(const Duration(seconds: 2), () async {
      var auth = await getAuth();
      print(auth.toString());
      if (auth == null) {
        Application.router.navigateTo(context, Routes.signIn,
            clearStack: true, replace: true, transition: TransitionType.fadeIn);
      } else {
        var data = gson.decode(auth);
        openCheckVerify(data);
      }
    });
  }

  Future openCheckVerify(dynamic data) async {
    var verify = await Navigator.of(context).push(new MaterialPageRoute<bool>(
        builder: (BuildContext context) {
          return new CustomWebView(
            state: CustomWebViewAction.verification,
            url: "",
            content: data,
          );
        },
        fullscreenDialog: true));
    if (verify != null) {
      if (verify) {
        gotoHome();
      } else {
        alertError(context, "Gagal melakukan verifikasi");
      }
    } else {
      alertError(context, "Gagal melakukan verifikasi");
    }
  }

  void gotoHome() {
    Application.router.navigateTo(context, Routes.home,
        clearStack: true, replace: true, transition: TransitionType.fadeIn);
  }

  @override
  Widget build(BuildContext context) {
    setDelay();

    return Scaffold(
        backgroundColor: ColorStyle.primary,
        body: BackgroundState(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(Drawable.face_logo, width: 125, height: 134),
            Image.asset(Drawable.text_logo, width: 125),
            Image.asset(Drawable.text_logo_description, width: 185),
          ],
        )));
  }
}
