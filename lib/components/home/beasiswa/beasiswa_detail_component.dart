import 'dart:convert';

import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'package:gson/gson.dart';

class BeasiswaDetailComponent extends StatefulWidget {
  BeasiswaDetailComponent({this.id});

  final String id;

  @override
  State createState() => BeasiswaDetailComponentState();
}

class BeasiswaDetailComponentState extends State<BeasiswaDetailComponent> {
  var width, height;
  String id = "";
  dynamic dataDetail;

  @override
  void initState() {
    super.initState();
    id = widget.id;

    loadNewsDetail(id);
  }

  void loadNewsDetail(String id) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.beasiswaDetail;
    body["id"] = id;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        if (data["beasiswa_detail"]["success"])
          setState(() {
            dataDetail = data["beasiswa_detail"]["results"];
          });
        else
          alertError(context, data["beasiswa_detail"]["message"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.white,
        body: (dataDetail == null)
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Container(
                    color: ColorStyle.white,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              height: 200,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                image: NetworkImage(dataDetail['thumbnail']),
                                fit: BoxFit.cover,
                                alignment: Alignment.center,
                              ))),
                          SizedBox(height: 16),
                          Container(
                              padding: EdgeInsets.only(left: 12, right: 12),
                              child: Text(dataDetail['title'],
                                  style: TextStyle(
                                      color: ColorStyle.black, fontSize: 20))),
                          SizedBox(height: 4),
                          Container(
                              padding: EdgeInsets.only(left: 12, right: 12),
                              child: Text(dataDetail["date"],
                                  style: TextStyle(
                                      color: ColorStyle.green, fontSize: 14))),
                          SizedBox(height: 4),
                          Container(
                              child: HtmlView(
                                data: dataDetail["content"],
                                scrollable: false,
                              ),
                              padding: EdgeInsets.all(12))
                        ])),
              ));
  }
}
