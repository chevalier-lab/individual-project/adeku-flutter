import 'dart:convert';

import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/CustomProducts.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:gson/gson.dart';

class ProductInternet extends State<CustomProducts> {
  var width, height;
  dynamic content, _balance, _selectedProduct;
  TextEditingController phoneNumberController;
  List<dynamic> _products = List();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    phoneNumberController = TextEditingController();

    content = widget.content;
    loadBalance();
  }

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  void loadBalance() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.balance;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State _balance
        if (data["balance"]["success"])
          setState(() {
            _balance = data["balance"]["results"];
          });
        else
          alertError(context, data["balance"]["message"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadVoucher() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    var phone =
        "0${(phoneNumberController.text.length > 3) ? phoneNumberController.text.substring(0, 3) : phoneNumberController.text}";
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    // body[KeyParams.requestsVouchersProduct] = content['id'];
    body[KeyParams.phone] = phone;
    body[KeyParams.category] = content['id'];
    body[KeyParams.requests0] = ValueParams.categoryByPrefix;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State _balance
        if (data['category_by_prefix']['success']) {
          loadVoucherList(data['category_by_prefix']['results'][0]);
        } else {
          setState(() {
            _isLoading = false;
          });
          alertError(context, data['category_by_prefix']["message"]);
        }
        // setState(() {
        //   _balance = data["results"];
        // });
      } else {
        setState(() {
          _isLoading = false;
        });
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        _isLoading = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadVoucherList(dynamic data) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    var phone = "0${phoneNumberController.text}";
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    // body[KeyParams.requestsVouchersProduct] = content['id'];
    body[KeyParams.phone] = phone;
    body[KeyParams.voucherId] = data['voucher_id'];
    body[KeyParams.requests0] = ValueParams.productByPrefix;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      setState(() {
        _isLoading = false;
      });
      if (data["success"]) {
        // Todo: Set State _balance
        if (data['product_by_prefix']['success']) {
          setState(() {
            _products = data['product_by_prefix']['results'];
          });
        } else
          alertError(context, data['product_by_prefix']["message"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        _isLoading = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadVoucherDetail(dynamic data) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    // body[KeyParams.requestsVouchersProduct] = content['id'];
    body[KeyParams.requestsProductDetailsId] = data['id'];
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State _balance
        if (data['product_details']['success']) {
          setState(() {
            _selectedProduct = data['product_details']['results'];
          });
          openBottomSheetDialog();
        } else
          alertError(context, data['product_by_prefix']["message"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ColorStyle.primary,
          title: Text(content['name'],
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
        ),
        body: RefreshIndicator(
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            SizedBox(height: 16),
            balance(),
            SizedBox(height: 16),
            formInput(),
            SizedBox(height: 8),
            listProduct()
          ]),
          onRefresh: () async {
            loadBalance();
            return;
          },
        ));
  }

  void openBottomSheetDialog() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Wrap(children: [
            Container(
                padding: EdgeInsets.all(16),
                color: ColorStyle.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          height: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              image: DecorationImage(
                                  image: NetworkImage(
                                      _selectedProduct['image_thumbnail'])))),
                      SizedBox(height: 16),
                      Text(_selectedProduct['name'],
                          style:
                              TextStyle(color: ColorStyle.black, fontSize: 20)),
                      SizedBox(height: 4),
                      Text(_selectedProduct['price_str'],
                          style: TextStyle(
                              color: ColorStyle.primary, fontSize: 16)),
                      SizedBox(height: 4),
                      Text("Provide: ${_selectedProduct['provider']['name']}",
                          style:
                              TextStyle(color: ColorStyle.text, fontSize: 16)),
                      FlatButton(
                          padding: EdgeInsets.only(
                              left: 0, right: 0, top: 16, bottom: 8),
                          onPressed: () {
                            setSelectedProduct(_selectedProduct);
                            setSelectedPhone("0${phoneNumberController.text}");
                            Navigator.pop(context);
                            goto(Routes.payment);
                          },
                          child: Container(
                            height: 56,
                            padding: EdgeInsets.all(16),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: ColorStyle.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(Values.buttonCheckout,
                                        style: TextStyle(
                                            color: ColorStyle.white))),
                                Text(_balance['balance_str'],
                                    style: TextStyle(color: ColorStyle.white)),
                                Icon(Icons.chevron_right,
                                    size: 24, color: ColorStyle.white)
                              ],
                            ),
                          ))
                    ]))
          ]);
        });
  }

  Widget listProduct() {
    return Expanded(
        child: (_isLoading)
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                padding: EdgeInsets.only(top: 8, bottom: 8, left: 4, right: 4),
                itemCount: _products.length,
                itemBuilder: (BuildContext contex, int index) {
                  var item = _products[index];
                  return GestureDetector(
                    onTap: () {
                      loadVoucherDetail(item);
                    },
                    child: Container(
                        margin: EdgeInsets.only(
                            top: 2, bottom: 2, left: 8, right: 8),
                        decoration: BoxDecoration(
                            color: ColorStyle.gray,
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: ListTile(
                          title: Text(item['name'],
                              style: TextStyle(color: ColorStyle.primaryDark)),
                          subtitle: Text(
                            "Harga: Rp ${item['price']}",
                            style: TextStyle(color: ColorStyle.text),
                          ),
                        )),
                  );
                }));
  }

  Widget formInput() {
    return Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Row(
          children: [
            Container(
                child: Text("+62",
                    style: TextStyle(color: ColorStyle.white, fontSize: 16)),
                padding:
                    EdgeInsets.only(left: 16, right: 16, top: 20, bottom: 20),
                decoration: BoxDecoration(
                    color: ColorStyle.brownLight,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(4),
                        bottomRight: Radius.circular(4)))),
            Expanded(
                child: PhoneNumberState(
                    controller: phoneNumberController,
                    onChange: () {
                      var phoneNumber = phoneNumberController.text;
                      if (phoneNumber.length >= 1) {
                        if (phoneNumber[0] == "0")
                          setState(() {
                            phoneNumberController.text = "";
                          });
                      }
                    })),
            GestureDetector(
              onTap: () {
                setState(() {
                  _isLoading = true;
                });
                loadVoucher();
              },
              child: Container(
                  child: Icon(Icons.search, color: ColorStyle.white, size: 24),
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
                  margin: EdgeInsets.only(left: 8),
                  decoration: BoxDecoration(
                      color: ColorStyle.green,
                      borderRadius: BorderRadius.circular(4))),
            ),
          ],
        ));
  }

  Widget balance() {
    return WrapBrownState(
        child: Container(
            padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
            child: GestureDetector(
                onTap: () {
                  goto(Routes.topUp);
                },
                child: Row(children: [
                  Expanded(
                      child: ItemDrawLeftState(
                          title: Align(
                              child: Text(
                                "Saldo",
                                style: TextStyle(color: ColorStyle.white),
                              ),
                              alignment: Alignment.bottomLeft),
                          subtitle: Align(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                (_balance != null)
                                    ? _balance['balance_str']
                                    : "Rp 0",
                                style: TextStyle(
                                    color: ColorStyle.white, fontSize: 12),
                              )),
                          icon: Container(
                              margin: EdgeInsets.only(right: 4),
                              child:
                                  Image.asset(Drawable.ic_wallet, width: 36)))),
                  Expanded(
                      child: ItemDrawLeftState(
                          title: Align(
                              child: Text("Top Up Saldo",
                                  style: TextStyle(color: ColorStyle.white)),
                              alignment: Alignment.bottomLeft),
                          icon: Container(
                              margin: EdgeInsets.only(right: 4),
                              child:
                                  Image.asset(Drawable.ic_top_up, width: 36))))
                ]))));
  }
}
