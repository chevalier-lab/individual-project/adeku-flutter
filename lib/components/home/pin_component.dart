import 'dart:async';
import 'dart:convert';

import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:gson/gson.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PinComponent extends StatefulWidget {
  @override
  State createState() => PinComponentState();
}

class PinComponentState extends State<PinComponent> {
  var width, height;
  TextEditingController pinController;

  StreamController<ErrorAnimationType> errorController;
  bool hasError = false, isDoChangePIN = false;
  String currentText = "";

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
    pinController = TextEditingController();
  }

  void doChangePIN() async {
    var auth = await getAuth();
    var data = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = data["token"];
    body[KeyParams.authUsername] = data["username"];
    body[KeyParams.pinSms] = currentText;
    await formData(Req.pin, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      setState(() {
        isDoChangePIN = false;
      });
      if (data["success"]) {
        gotoHome();
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        isDoChangePIN = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void gotoHome() {
    Application.router.navigateTo(context, Routes.home,
        clearStack: true, replace: true, transition: TransitionType.fadeIn);
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ColorStyle.primary,
          title: Text(Values.titlePIN,
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Image.asset(Drawable.undraw_forgot_password_gi2d, width: 205),
          SizedBox(height: 16),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
              child: PinCodeTextField(
                  length: 4,
                  enableActiveFill: true,
                  errorAnimationController: errorController,
                  controller: pinController,
                  textInputType: TextInputType.number,
                  pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      fieldHeight: 60,
                      fieldWidth: 60,
                      inactiveFillColor: ColorStyle.white,
                      activeFillColor: ColorStyle.white),
                  animationDuration: Duration(milliseconds: 300),
                  textStyle: TextStyle(fontSize: 20),
                  backgroundColor: Colors.transparent,
                  onChanged: (value) {
                    print(value);
                    setState(() {
                      currentText = value;
                    });
                  })),
        ]),
        bottomNavigationBar: FlatButton(
            onPressed: () {
              setState(() {
                isDoChangePIN = true;
              });
              doChangePIN();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              margin: EdgeInsets.only(top: 16, bottom: 16),
              padding: EdgeInsets.all(16),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ColorStyle.green,
                  borderRadius: BorderRadius.all(Radius.circular(16))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    Values.buttonSave,
                    style: TextStyle(color: ColorStyle.white),
                  ),
                  (!isDoChangePIN)
                      ? Container()
                      : SizedBox(
                          width: 16,
                        ),
                  (!isDoChangePIN)
                      ? Container()
                      : SizedBox(
                          width: 24,
                          height: 24,
                          child: CircularProgressIndicator())
                ],
              ),
            )));
  }
}
