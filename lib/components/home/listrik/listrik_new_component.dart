import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListrikNewComponent extends StatefulWidget {
  @override
  State createState() => ListrikNewComponentState();
}

class ListrikNewComponentState extends State<ListrikNewComponent> {
  var width, height;
  TextEditingController saldoController,
      operatorController,
      nominalController,
      phoneController;

  Widget topBar() {
    return BoxBrownNavContentState(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(Values.titleListrik,
                  textAlign: TextAlign.start,
                  style:
                      TextStyle(fontSize: 18, color: ColorStyle.primaryDark)),
              alignment: Alignment.bottomLeft),
          iconTheme: IconThemeData(color: ColorStyle.primaryDark),
        ),
        child: Container(
            margin: EdgeInsets.only(top: 80, bottom: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.explore,
                  size: 64,
                  color: ColorStyle.primaryDark,
                ),
                Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Listrik",
                      style: TextStyle(
                          color: ColorStyle.primaryDark, fontSize: 20),
                    )),
                SizedBox(
                  height: 8,
                ),
              ],
            )));
  }

  @override
  void initState() {
    super.initState();
    saldoController = TextEditingController();
    operatorController = TextEditingController();
    nominalController = TextEditingController();
    phoneController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.white,
        body: SingleChildScrollView(
            child: Column(
          children: [
            topBar(),
            SizedBox(
              height: 16,
            ),
            Container(
                margin: EdgeInsets.only(top: 8, left: 16, right: 16),
                child: NumberState(
                    hint: "Saldo Anda",
                    controller: saldoController,
                    onChange: () {})),
            Container(
                margin: EdgeInsets.only(top: 8, left: 16, right: 16),
                child: NumberState(
                    hint: "Operator",
                    controller: saldoController,
                    onChange: () {})),
            Container(
                margin: EdgeInsets.only(top: 8, left: 16, right: 16),
                child: NumberState(
                    hint: "Nominal",
                    controller: saldoController,
                    onChange: () {})),
            Container(
                margin: EdgeInsets.only(top: 8, left: 16, right: 16),
                child: NumberState(
                    hint: "Nomor Telepon",
                    controller: saldoController,
                    onChange: () {}))
          ],
        )),
        bottomNavigationBar: FlatButton(
            onPressed: () {},
            child: Container(
              width: width,
              height: 56,
              margin: EdgeInsets.only(top: 16, bottom: 16),
              padding: EdgeInsets.all(16),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ColorStyle.primaryDark,
                  borderRadius: BorderRadius.all(Radius.circular(16))),
              child: Text(
                Values.buttonSend,
                style: TextStyle(color: ColorStyle.white),
              ),
            )));
  }
}
