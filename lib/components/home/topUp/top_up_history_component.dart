import 'dart:convert';

import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/CustomWebView.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gson/gson.dart';

class TopUpHistoryComponent extends StatefulWidget {
  TopUpHistoryComponent();
  @override
  State createState() => TopUpHistoryComponentState();
}

class TopUpHistoryComponentState extends State<TopUpHistoryComponent> {
  var width, height;
  List<dynamic> _topUpHistory = [];

  Widget topBar() {
    return BoxBrownNavContentState(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text("Riwayat Top Up",
                  textAlign: TextAlign.start,
                  style:
                      TextStyle(fontSize: 18, color: ColorStyle.primaryDark)),
              alignment: Alignment.bottomLeft),
          iconTheme: IconThemeData(color: ColorStyle.primaryDark),
        ),
        child: Container());
  }

  @override
  void initState() {
    super.initState();
    loadDeposit();
  }

  void loadDeposit() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requestsDepositHistoryPage] = 1;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        setState(() {
          _topUpHistory = data['deposit_history']['results'];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadDepositDetail(int id) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requestsDepositDetailsId] = id;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        openDetailDeposit(data['deposit_details']['results']);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  Future openDetailDeposit(dynamic data) async {
    await Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new CustomWebView(
            state: CustomWebViewAction.detailDeposit,
            url: "",
            content: data,
          );
        },
        fullscreenDialog: true));
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.white,
        body: Column(children: [
          topBar(),
          SizedBox(
            height: 16,
          ),
          Expanded(child: topUpList())
        ]));
  }

  Widget topUpList() {
    return (_topUpHistory.length == 0)
        ? Center(child: CircularProgressIndicator())
        : ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 0, bottom: 8, left: 4, right: 4),
            itemCount: _topUpHistory.length,
            itemBuilder: (BuildContext contex, int position) {
              var item = _topUpHistory[position];
              return GestureDetector(
                  onTap: () {
                    loadDepositDetail(item['id']);
                  },
                  child: Container(
                      padding: EdgeInsets.all(16),
                      margin: EdgeInsets.only(bottom: 8, left: 8, right: 8),
                      decoration: BoxDecoration(
                          color: ColorStyle.brown,
                          borderRadius: BorderRadius.circular(8)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            item['payment'],
                            style: TextStyle(
                                color: ColorStyle.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            item['date'],
                            style: TextStyle(color: ColorStyle.white),
                          ),
                          Text(
                            Values.getStatus(item['status']),
                            style: TextStyle(color: ColorStyle.white),
                          )
                        ],
                      )));
            });
  }
}
