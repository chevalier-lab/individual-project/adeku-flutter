import 'dart:convert';

import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gson/gson.dart';

class TopUpComponent extends StatefulWidget {
  @override
  State createState() => TopUpComponentState();
}

class TopUpComponentState extends State<TopUpComponent> {
  var width, height, _balance;
  List<dynamic> _payments = [];

  List<Image> listPayment = [
    Image.asset(Drawable.ic_gopay, width: 48),
    Image.asset(Drawable.ic_atm, width: 48),
    Image.asset(Drawable.ic_kaspro, width: 48),
  ];

  @override
  void initState() {
    super.initState();
    loadListPayment();
    loadBalance();
  }

  void loadBalance() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.balance;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State _balance
        setState(() {
          _balance = data["results"];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadListPayment() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.payments;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
        List<dynamic> temp = data['payments']['results'];
        temp.removeAt(0);
        setState(() {
          _payments = temp;
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  Widget topBar() {
    return BoxBrownNavContentState(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(Values.titleTopUp,
                  textAlign: TextAlign.start,
                  style:
                      TextStyle(fontSize: 18, color: ColorStyle.primaryDark)),
              alignment: Alignment.bottomLeft),
          iconTheme: IconThemeData(color: ColorStyle.primaryDark),
          actions: [
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Application.router.navigateTo(context, Routes.topUpHistory,
                        clearStack: false,
                        replace: false,
                        transition: TransitionType.fadeIn);
                  },
                  child: Icon(
                    Icons.history,
                    size: 26.0,
                    color: ColorStyle.white,
                  ),
                ))
          ],
        ),
        child: Container(
            margin: EdgeInsets.only(top: 80, bottom: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.account_balance_wallet,
                  size: 64,
                  color: ColorStyle.primaryDark,
                ),
                Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Saldo Anda",
                      style: TextStyle(color: ColorStyle.primaryDark),
                    )),
                SizedBox(
                  height: 8,
                ),
                Align(
                    alignment: Alignment.center,
                    child: Text(
                      (_balance != null) ? _balance['balance_str'] : "Rp 0",
                      style: TextStyle(
                          color: ColorStyle.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                    )),
                SizedBox(
                  height: 8,
                ),
              ],
            )));
  }

  Widget listTopUp() {
    return GridView.count(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 24, right: 24, top: 16),
        crossAxisCount: listPayment.length,
        children: List.generate(listPayment.length, (position) {
          var item = listPayment[position];
          return GestureDetector(
              onTap: () {
                var type = (position == 0)
                    ? "Gopay"
                    : (position == 1) ? "ATM" : "Kaspro";
                Application.router.navigateTo(context, Routes.topUp + "/$type",
                    clearStack: false,
                    replace: false,
                    transition: TransitionType.fadeIn);
              },
              child: Container(
                  padding: EdgeInsets.all(16),
                  margin: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: ColorStyle.brown,
                      borderRadius: BorderRadius.circular(8)),
                  child: item));
        }));
  }

  Widget listTopUpVertical() {
    return (_payments.length == 0)
        ? Center(child: CircularProgressIndicator())
        : ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            padding: EdgeInsets.only(top: 24, bottom: 8, left: 4, right: 4),
            itemCount: _payments.length,
            itemBuilder: (BuildContext contex, int position) {
              var item = _payments[position];
              return GestureDetector(
                  onTap: () {
                    var type = item['id'];
                    Application.router.navigateTo(
                        context, Routes.topUp + "/$type",
                        clearStack: false,
                        replace: false,
                        transition: TransitionType.fadeIn);
                  },
                  child: Container(
                      padding: EdgeInsets.all(16),
                      margin: EdgeInsets.only(bottom: 8, left: 8, right: 8),
                      decoration: BoxDecoration(
                          color: ColorStyle.brown,
                          borderRadius: BorderRadius.circular(8)),
                      child: Text(
                        item['name'],
                        style: TextStyle(color: ColorStyle.white),
                      )));
            });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.white,
        body: Column(
          children: [
            topBar(),
            Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 24),
                child: Align(
                    alignment: Alignment.center,
                    child: Text(Values.descriptionTopUp,
                        textAlign: TextAlign.center))),
            listTopUpVertical(),
            Expanded(child: Image.asset(Drawable.top_up, width: 120))
          ],
        ));
  }
}
