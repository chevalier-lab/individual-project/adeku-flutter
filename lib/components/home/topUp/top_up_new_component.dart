import 'dart:convert';

import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/CustomWebView.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gson/gson.dart';

class TopUpNewComponent extends StatefulWidget {
  TopUpNewComponent({@required this.title});
  final String title;
  @override
  State createState() => TopUpNewComponentState();
}

class TopUpNewComponentState extends State<TopUpNewComponent> {
  var width, height;
  TextEditingController topUpController;
  bool isDoTopUp = false;

  Widget topBar() {
    return BoxBrownNavContentState(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(
                  "Top Up via ${widget.title.replaceAll('_', ' ').toUpperCase()}",
                  textAlign: TextAlign.start,
                  style:
                      TextStyle(fontSize: 18, color: ColorStyle.primaryDark)),
              alignment: Alignment.bottomLeft),
          iconTheme: IconThemeData(color: ColorStyle.primaryDark),
        ),
        child: Container());
  }

  @override
  void initState() {
    super.initState();
    topUpController = TextEditingController();
  }

  void loadDepositDetail(int id) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requestsDepositDetailsId] = id;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        openDetailDeposit(data['deposit_details']['results']);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  Future openDetailDeposit(dynamic data) async {
    await Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new CustomWebView(
            state: CustomWebViewAction.detailDeposit,
            url: "",
            content: data,
          );
        },
        fullscreenDialog: true));
  }

  void doDeposit() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.payment] = widget.title.toLowerCase();
    body[KeyParams.amount] = topUpController.text;
    await formData(Req.depositAdd, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      setState(() {
        isDoTopUp = false;
      });
      if (data["success"] && data["status"]) {
        alertSuccess(context, "Berhasil melakukan top up");
        setState(() {
          topUpController.clear();
        });
        loadDepositDetail(data["results"]["id"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        isDoTopUp = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.white,
        body: Column(
          children: [
            topBar(),
            SizedBox(
              height: 16,
            ),
            Container(
                margin: EdgeInsets.all(16),
                child: Column(children: [
                  Image.asset(
                    Drawable.undraw_transfer_money_rywa,
                    width: 200,
                  ),
                  SizedBox(height: 16),
                  NumberState(
                      hint: "Jumlah Deposit",
                      controller: topUpController,
                      onChange: () {}),
                  SizedBox(height: 8),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Text("Minimum: Rp. 100.000"),
                  ),
                  SizedBox(height: 16)
                ]))
          ],
        ),
        bottomNavigationBar: FlatButton(
            onPressed: () {
              if (!isDoTopUp) {
                setState(() {
                  isDoTopUp = true;
                });
                doDeposit();
              }
            },
            child: Container(
              width: width,
              height: 56,
              margin: EdgeInsets.only(top: 16, bottom: 16),
              padding: EdgeInsets.all(16),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ColorStyle.primaryDark,
                  borderRadius: BorderRadius.all(Radius.circular(16))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    Values.buttonSend,
                    style: TextStyle(color: ColorStyle.white),
                  ),
                  (!isDoTopUp)
                      ? Container()
                      : SizedBox(
                          width: 16,
                        ),
                  (!isDoTopUp)
                      ? Container()
                      : SizedBox(
                          width: 24,
                          height: 24,
                          child: CircularProgressIndicator())
                ],
              ),
            )));
  }
}
