import 'dart:convert';

import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:gson/gson.dart';

class DonasiComponent extends StatefulWidget {
  @override
  State createState() => DonasiComponentState();
}

class DonasiComponentState extends State<DonasiComponent> {
  var width, height;
  int currentBottomIndex = 0;
  dynamic _selectedDonation, _profile;
  TextEditingController phoneNumberController;
  List<dynamic> _donation = [];
  List<dynamic> listBerita = [
    {
      'title': "Ade Utami Ibnu Temui Gubernur",
      'description': "Loremp ipsum color at amet"
    },
    {
      'title': "Gubernur Temui Ade Utami Ibnu",
      'description': "Loremp ipsum color at amet"
    },
    {
      'title': "Silaturahmi Bersama",
      'description': "Loremp ipsum color at amet"
    },
    {'title': "Bersama Kita Bisa", 'description': "Loremp ipsum color at amet"}
  ];

  @override
  void initState() {
    super.initState();

    phoneNumberController = TextEditingController();

    loadProfile();
    loadProducts();
  }

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  void loadProfile() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.accountDetails;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
        setState(() {
          _profile = data['account_details']['results'];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadProducts() async {
    var auth = await getAuth();
    var selectedCategoryDonation = await getSelectedPPOBCategoryDonation();
    var authData = gson.decode(auth);
    var categoryDonation = gson.decode(selectedCategoryDonation);

    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requestsVouchersProduct] = categoryDonation['id'];

    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        setState(() {
          _donation = data["vouchers"]["results"];
        });
        // Todo: Set State Profile Checker
        // List<dynamic> category = data['products']['results'];
        // setSelectedPPOBCategoryDonation(category[category.length - 1]);
        // category.removeLast();
        // setState(() {
        //   _categories = category;
        // });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void openBottomSheetDialog() {
    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext context1, Animation animation,
            Animation secondaryAnimation) {
          return Scaffold(
              appBar: AppBar(
                title: Text("Detail Donasi"),
              ),
              body: SingleChildScrollView(
                  child: Container(
                      padding: EdgeInsets.all(16),
                      color: ColorStyle.white,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                height: 200,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    image: DecorationImage(
                                        image: NetworkImage(_selectedDonation[
                                            'image_thumbnail']),
                                        fit: BoxFit.fill))),
                            SizedBox(height: 16),
                            Text(_selectedDonation['name'],
                                style: TextStyle(
                                    color: ColorStyle.black, fontSize: 20)),
                            SizedBox(height: 4),
                            Text(_selectedDonation['price_str'],
                                style: TextStyle(
                                    color: ColorStyle.primary, fontSize: 16)),
                            SizedBox(height: 4),
                            Text(
                                "Provide: ${_selectedDonation['provider']['name']}",
                                style: TextStyle(
                                    color: ColorStyle.text, fontSize: 16)),
                            SizedBox(height: 4),
                            NumberState(
                                hint: "Nominal Donasi",
                                controller: phoneNumberController,
                                onChange: () {}),
                          ]))),
              bottomNavigationBar: FlatButton(
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 16),
                  onPressed: () async {
                    var selectedCategoryDonation =
                        await getSelectedPPOBCategoryDonation();
                    var categoryDonation =
                        gson.decode(selectedCategoryDonation);

                    setSelectedProduct(_selectedDonation);
                    if (_profile != null) setSelectedPhone(_profile['phone']);
                    setSelectedPPOBCategory(categoryDonation);
                    setSelectedDonation(phoneNumberController.text);
                    Navigator.pop(context);
                    goto(Routes.payment);
                  },
                  child: Container(
                    height: 56,
                    padding: EdgeInsets.all(16),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: ColorStyle.green,
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(Values.buttonCheckout,
                                style: TextStyle(color: ColorStyle.white))),
                        Icon(Icons.chevron_right,
                            size: 24, color: ColorStyle.white)
                      ],
                    ),
                  )));
        });
  }

  Widget topBar() {
    return BoxOrangeNavContentState(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(Values.titleDonasi,
                  textAlign: TextAlign.start,
                  style:
                      TextStyle(fontSize: 18, color: ColorStyle.primaryDark)),
              alignment: Alignment.bottomLeft),
          iconTheme: IconThemeData(color: ColorStyle.primaryDark),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                  margin: EdgeInsets.only(top: 80, left: 32, right: 32),
                  alignment: Alignment.bottomRight,
                  child: Image.asset(
                    Drawable.face_logo,
                    width: 140,
                  )),
            ),
            Container(
              margin: EdgeInsets.only(top: 80, bottom: 16, left: 32, right: 32),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Image.asset(Drawable.ic_donasi),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Align(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                          "Berikan Donasi Terbaik Anda",
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold),
                        ))
                  ]),
            )
          ],
        ));
  }

  Widget beritaBar() {
    return ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 16, bottom: 16, left: 0, right: 0),
        itemCount: _donation.length,
        itemBuilder: (BuildContext context, int position) {
          var item = _donation[position];
          return GestureDetector(
              onTap: () {
                setState(() {
                  _selectedDonation = item;
                });

                openBottomSheetDialog();
              },
              child: Column(
                children: [
                  Container(
                      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                      child: Row(
                        children: [
                          Container(
                              width: 96,
                              height: 96,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: ColorStyle.black, width: 1),
                                  image: DecorationImage(
                                      image:
                                          NetworkImage(item['image_thumbnail']),
                                      fit: BoxFit.cover))),
                          Expanded(
                              child: Column(
                            children: [
                              Container(
                                  padding: EdgeInsets.only(left: 8),
                                  alignment: Alignment.bottomLeft,
                                  child: Text(
                                    item["name"],
                                    style: TextStyle(
                                        fontSize: 16, color: ColorStyle.black),
                                  )),
                              Container(
                                  padding: EdgeInsets.only(left: 8),
                                  alignment: Alignment.bottomLeft,
                                  child: Text(item["price_str"]))
                            ],
                          ))
                        ],
                      )),
                  Divider(color: ColorStyle.gray, height: 1)
                ],
              ));
        });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.white,
        body: SingleChildScrollView(
            child: Column(children: [topBar(), beritaBar()])));
  }
}
