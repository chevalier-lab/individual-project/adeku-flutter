import 'dart:convert';

import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/CustomProducts.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:gson/gson.dart';

class ProductDonasi extends State<CustomProducts> {
  var width, height;
  dynamic content, _balance;

  @override
  void initState() {
    super.initState();

    content = widget.content;
    loadBalance();
  }

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  void loadBalance() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.balance;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State _balance
        setState(() {
          _balance = data["results"];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ColorStyle.primary,
          title: Text(content['name'],
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          SizedBox(height: 16),
          balance(),
          SizedBox(height: 16),
        ]));
  }

  Widget balance() {
    return WrapBrownState(
        child: Container(
            padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
            child: GestureDetector(
                onTap: () {
                  goto(Routes.topUp);
                },
                child: Row(children: [
                  Expanded(
                      child: ItemDrawLeftState(
                          title: Align(
                              child: Text(
                                "Saldo",
                                style: TextStyle(color: ColorStyle.white),
                              ),
                              alignment: Alignment.bottomLeft),
                          subtitle: Align(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                (_balance != null)
                                    ? _balance['balance_str']
                                    : "Rp 0",
                                style: TextStyle(
                                    color: ColorStyle.white, fontSize: 12),
                              )),
                          icon: Container(
                              margin: EdgeInsets.only(right: 4),
                              child:
                                  Image.asset(Drawable.ic_wallet, width: 36)))),
                  Expanded(
                      child: ItemDrawLeftState(
                          title: Align(
                              child: Text("Top Up Saldo",
                                  style: TextStyle(color: ColorStyle.white)),
                              alignment: Alignment.bottomLeft),
                          icon: Container(
                              margin: EdgeInsets.only(right: 4),
                              child:
                                  Image.asset(Drawable.ic_top_up, width: 36))))
                ]))));
  }
}
