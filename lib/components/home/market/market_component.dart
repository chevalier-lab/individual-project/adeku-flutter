import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/images_constant.dart';

class MarketComponent extends StatefulWidget {
  @override
  State createState() => MarketComponentState();
}

class MarketComponentState extends State<MarketComponent> {
  var width, height;

  int currentBottomIndex = 0;

  List<dynamic> listBerita = [
    {
      'title': "Ade Utami Ibnu Temui Gubernur",
      'description': "Loremp ipsum color at amet"
    },
    {
      'title': "Gubernur Temui Ade Utami Ibnu",
      'description': "Loremp ipsum color at amet"
    },
    {
      'title': "Silaturahmi Bersama",
      'description': "Loremp ipsum color at amet"
    },
    {'title': "Bersama Kita Bisa", 'description': "Loremp ipsum color at amet"}
  ];

  List<String> listMenu = ["Pulsa", "Internet", "Listrik", "BPJS"];
  List<Image> listMenuImage = [
    Image.asset(Drawable.ic_pulsa, width: 36),
    Image.asset(Drawable.ic_internet, width: 36),
    Image.asset(Drawable.ic_listrik, width: 36),
    Image.asset(Drawable.ic_bpjs, width: 36)
  ];
  List<String> listMenuAction = [
    Routes.pulsaNew,
    Routes.internetNew,
    Routes.listrikNew,
    Routes.bpjsNew
  ];

  Widget balanceWidget() {
    return GestureDetector(
        onTap: () {
          goto(Routes.topUp);
        },
        child: Row(children: [
          Expanded(
              child: ItemDrawLeftState(
                  title: Align(
                      child: Text(
                        "Saldo",
                        style: TextStyle(color: ColorStyle.white),
                      ),
                      alignment: Alignment.bottomLeft),
                  subtitle: Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        "Rp. 1.000.000",
                        style: TextStyle(color: ColorStyle.white, fontSize: 12),
                      )),
                  icon: Container(
                      margin: EdgeInsets.only(right: 4),
                      child: Image.asset(Drawable.ic_wallet, width: 36)))),
          Expanded(
              child: ItemDrawLeftState(
                  title: Align(
                      child: Text("Top Up Saldo",
                          style: TextStyle(color: ColorStyle.white)),
                      alignment: Alignment.bottomLeft),
                  icon: Container(
                      margin: EdgeInsets.only(right: 4),
                      child: Image.asset(Drawable.ic_top_up, width: 36))))
        ]));
  }

  Widget menuWidget() {
    return GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: listMenu.length,
        shrinkWrap: mounted,
        padding: EdgeInsets.only(top: 16),
        children: List.generate(listMenu.length, (position) {
          var item = listMenu[position];
          var itemImage = listMenuImage[position];
          var itemAction = listMenuAction[position];
          return GestureDetector(
              onTap: () {
                goto(itemAction);
              },
              child: Column(
                children: [
                  Container(
                      decoration: BoxDecoration(
                          color: ColorStyle.primary,
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: itemImage),
                  SizedBox(height: 4),
                  Text(item, style: TextStyle(color: ColorStyle.white))
                ],
              ));
        }));
  }

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  Widget topBar() {
    return BoxOrangeNavContentState(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(Values.titleMarket,
                  textAlign: TextAlign.start,
                  style:
                      TextStyle(fontSize: 18, color: ColorStyle.primaryDark)),
              alignment: Alignment.bottomLeft),
          iconTheme: IconThemeData(color: ColorStyle.primaryDark),
        ),
        child: Stack(
          children: [
            Align(
                alignment: Alignment.bottomRight,
                child: Column(
                  children: [
                    Container(
                        margin: EdgeInsets.only(top: 80, left: 32, right: 32),
                        alignment: Alignment.bottomRight,
                        child: Image.asset(
                          Drawable.face_logo,
                          width: 140,
                        )),
                    Container(
                      margin: EdgeInsets.only(bottom: 16),
                      child: WrapBrownState(
                          child: Container(
                        padding: EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: Column(children: [
                          balanceWidget(),
                          SizedBox(height: 16),
                          Divider(color: ColorStyle.accent, height: 1),
                          menuWidget()
                        ]),
                      )),
                    ),
                  ],
                )),
            Container(
              margin: EdgeInsets.only(top: 80, bottom: 16, left: 32, right: 32),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Image.asset(Drawable.ic_market),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Align(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                          "Adeku \nMarket Place",
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold),
                        ))
                  ]),
            ),
          ],
        ));
  }

  Widget beritaBar() {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.only(top: 16, bottom: 16, left: 0, right: 0),
        itemCount: listBerita.length,
        itemBuilder: (BuildContext context, int position) {
          var item = listBerita[position];
          return GestureDetector(
              onTap: () {},
              child: ListTile(
                leading: Icon(
                  Icons.image,
                  size: 80,
                ),
                title: Text(item["title"]),
                subtitle: Text(item["description"]),
              ));
        });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.white,
        body: SingleChildScrollView(
            child: Column(children: [topBar(), beritaBar()])));
  }
}
