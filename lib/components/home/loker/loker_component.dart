import 'dart:convert';

import 'package:adekuflutter/components/home/loker/loker_detail_component.dart';
import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:gson/gson.dart';

class LokerComponent extends StatefulWidget {
  @override
  State createState() => LokerComponentState();
}

class LokerComponentState extends State<LokerComponent> {
  var width, height;

  int currentBottomIndex = 0;

  List<dynamic> listBerita = [];

  @override
  void initState() {
    super.initState();

    loadLoker();
  }

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  void loadLoker() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.lowongan;
    body["page"] = 1;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        if (data["lowongan"]["success"])
          setState(() {
            listBerita = data["lowongan"]["result"];
          });
        else
          alertError(context, data["lowongan"]["message"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadLokerDetail(String id) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.lowonganDetail;
    body["id"] = id;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
        // List<dynamic> category = data['products']['results'];
        // setSelectedPPOBCategoryDonation(category[category.length - 1]);
        // category.removeLast();
        // setState(() {
        //   _categories = category;
        // });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  Future openDetail(dynamic data) async {
    var status = await Navigator.of(context).push(new MaterialPageRoute<bool>(
        builder: (BuildContext context) {
          return new LokerDetailComponent(id: data['id']);
        },
        fullscreenDialog: true));
  }

  Widget topBar() {
    return BoxOrangeNavContentState(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(Values.titleLoker,
                  textAlign: TextAlign.start,
                  style:
                      TextStyle(fontSize: 18, color: ColorStyle.primaryDark)),
              alignment: Alignment.bottomLeft),
          iconTheme: IconThemeData(color: ColorStyle.primaryDark),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                  margin: EdgeInsets.only(top: 80, left: 32, right: 32),
                  alignment: Alignment.bottomRight,
                  child: Image.asset(
                    Drawable.face_logo,
                    width: 140,
                  )),
            ),
            Container(
              margin: EdgeInsets.only(top: 80, bottom: 16, left: 32, right: 32),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Image.asset(Drawable.ic_loker),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Align(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                          "Informasi \nLowongan Kerja",
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold),
                        ))
                  ]),
            )
          ],
        ));
  }

  Widget beritaBar() {
    return ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 16, bottom: 16, left: 0, right: 0),
        itemCount: listBerita.length,
        itemBuilder: (BuildContext context, int position) {
          var item = listBerita[position];
          return GestureDetector(
              onTap: () {
                openDetail(item);
              },
              child: Column(
                children: [
                  Container(
                      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                      child: Row(
                        children: [
                          Container(
                              width: 96,
                              height: 96,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImage(item['thumbnail']),
                                      fit: BoxFit.cover))),
                          Expanded(
                              child: Column(
                            children: [
                              Container(
                                  padding: EdgeInsets.only(left: 8),
                                  alignment: Alignment.bottomLeft,
                                  child: Text(
                                    item["title"],
                                    style: TextStyle(
                                        fontSize: 16, color: ColorStyle.black),
                                  )),
                              Container(
                                  padding: EdgeInsets.only(left: 8),
                                  alignment: Alignment.bottomLeft,
                                  child: Text(item["author"])),
                              Container(
                                  padding: EdgeInsets.only(left: 8),
                                  alignment: Alignment.bottomLeft,
                                  child: Text(item["date"]))
                            ],
                          ))
                        ],
                      )),
                  Divider(color: ColorStyle.gray, height: 1)
                ],
              ));
        });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.white,
        body: SingleChildScrollView(
            child: Column(children: [topBar(), beritaBar()])));
  }
}
