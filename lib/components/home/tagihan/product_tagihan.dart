import 'dart:convert';

import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/CustomProducts.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:gson/gson.dart';

class ProductTagihan extends State<CustomProducts> {
  var width, height;
  dynamic content, _balance, _selectedProduct, _profile;
  TextEditingController phoneNumberController;
  List<dynamic> _products = List();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    phoneNumberController = TextEditingController();

    content = widget.content;
    loadBalance();
    loadProduct();
    loadProfile();
  }

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  void loadProfile() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.accountDetails;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
        setState(() {
          _profile = data['account_details']['results'];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadBalance() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.balance;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State _balance
        if (data["balance"]["success"])
          setState(() {
            _balance = data["balance"]["results"];
          });
        else
          alertError(context, data["balance"]["message"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadProduct() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    print(authData.toString());
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    // body[KeyParams.requestsVouchersProduct] = content['id'];
    body[KeyParams.requestsVouchersProduct] = content['id'];
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        List<dynamic> items = data["vouchers"]["results"];
        items.forEach((element) {
          if (element['code'].substring(0, 3) == "CEK") {
            setState(() {
              _products.add(element);
            });
          }
        });
      } else {
        setState(() {
          _isLoading = false;
        });
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        _isLoading = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void doCekTagihan(dynamic item, String idPLGN) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    // body[KeyParams.requestsVouchersProduct] = content['id'];
    body[KeyParams.requests0] = ValueParams.cekTagihan;
    body[KeyParams.idVoucher] = item['id'];
    body[KeyParams.idPlgn] = idPLGN;
    body[KeyParams.phone] = _profile["phone"];
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        if (data["cek_tagihan"]["is_waiting_server"])
          doCekTagihan(item, idPLGN);
        else {
          if (!data["cek_tagihan"]["success"])
            alertSuccess(context, data["cek_tagihan"]["message"]);
        }
        // List<dynamic> items = data["vouchers"]["results"];
        // items.forEach((element) {
        //   if (element['code'].substring(0, 3) == "CEK") {
        //     setState(() {
        //       _products.add(element);
        //     });
        //   }
        // });
      } else {
        setState(() {
          _isLoading = false;
        });
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        _isLoading = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ColorStyle.primary,
          title: Text(content['name'],
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
        ),
        body: RefreshIndicator(
          child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
            SizedBox(height: 16),
            balance(),
            // SizedBox(height: 16),
            // formInput(),
            SizedBox(height: 8),
            listProduct()
          ]),
          onRefresh: () async {
            loadBalance();
            return;
          },
        ));
  }

  void openBottomSheetDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Cek Tagihan'),
            content: SingleChildScrollView(
                child: Container(
                    color: ColorStyle.white,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("ID Pelanggan",
                              style: TextStyle(
                                  color: ColorStyle.text, fontSize: 16)),
                          SizedBox(height: 8),
                          NumberState(
                              hint: "ID Pelanggan",
                              controller: phoneNumberController,
                              onChange: () {}),
                          FlatButton(
                              padding: EdgeInsets.only(
                                  left: 0, right: 0, top: 16, bottom: 8),
                              onPressed: () {
                                // setSelectedProduct(_selectedProduct);
                                // setSelectedPhone("0${phoneNumberController.text}");
                                Navigator.pop(context);
                                doCekTagihan(_selectedProduct,
                                    phoneNumberController.text);
                              },
                              child: Container(
                                height: 56,
                                padding: EdgeInsets.all(16),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: ColorStyle.green,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8))),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Text("Cek Tagihan",
                                            style: TextStyle(
                                                color: ColorStyle.white))),
                                    Icon(Icons.chevron_right,
                                        size: 24, color: ColorStyle.white)
                                  ],
                                ),
                              ))
                        ]))),
          );
        });
  }

  Widget listProduct() {
    return Expanded(
        child: (_isLoading)
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                padding: EdgeInsets.only(top: 8, bottom: 8, left: 4, right: 4),
                itemCount: _products.length,
                itemBuilder: (BuildContext contex, int index) {
                  var item = _products[index];
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedProduct = item;
                      });
                      openBottomSheetDialog();
                    },
                    child: Container(
                        margin: EdgeInsets.only(
                            top: 2, bottom: 2, left: 8, right: 8),
                        decoration: BoxDecoration(
                            color: ColorStyle.gray,
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: ListTile(
                          title: Text(item['name'],
                              style: TextStyle(color: ColorStyle.primaryDark)),
                          subtitle: Text(
                            "Provider: ${item['provider']['name']}",
                            style: TextStyle(color: ColorStyle.text),
                          ),
                        )),
                  );
                }));
  }

  Widget balance() {
    return WrapBrownState(
        child: Container(
            padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
            child: GestureDetector(
                onTap: () {
                  goto(Routes.topUp);
                },
                child: Row(children: [
                  Expanded(
                      child: ItemDrawLeftState(
                          title: Align(
                              child: Text(
                                "Saldo",
                                style: TextStyle(color: ColorStyle.white),
                              ),
                              alignment: Alignment.bottomLeft),
                          subtitle: Align(
                              alignment: Alignment.bottomLeft,
                              child: Text(
                                (_balance != null)
                                    ? _balance['balance_str']
                                    : "Rp 0",
                                style: TextStyle(
                                    color: ColorStyle.white, fontSize: 12),
                              )),
                          icon: Container(
                              margin: EdgeInsets.only(right: 4),
                              child:
                                  Image.asset(Drawable.ic_wallet, width: 36)))),
                  Expanded(
                      child: ItemDrawLeftState(
                          title: Align(
                              child: Text("Top Up Saldo",
                                  style: TextStyle(color: ColorStyle.white)),
                              alignment: Alignment.bottomLeft),
                          icon: Container(
                              margin: EdgeInsets.only(right: 4),
                              child:
                                  Image.asset(Drawable.ic_top_up, width: 36))))
                ]))));
  }
}
