import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/images_constant.dart';

class AspirasiSuccessComponent extends StatefulWidget {
  @override
  State createState() => AspirasiSuccessComponentState();
}

class AspirasiSuccessComponentState extends State<AspirasiSuccessComponent> {
  var width, height;

  int currentBottomIndex = 0;

  List<String> listMenu = ["Pulsa", "Internet", "Listrik", "BPJS"];
  List<Image> listMenuImage = [
    Image.asset(Drawable.ic_pulsa, width: 36),
    Image.asset(Drawable.ic_internet, width: 36),
    Image.asset(Drawable.ic_listrik, width: 36),
    Image.asset(Drawable.ic_bpjs, width: 36)
  ];
  List<String> listMenuAction = [
    Routes.pulsaNew,
    Routes.internetNew,
    Routes.listrikNew,
    Routes.bpjsNew
  ];

  List<String> listContentMenu = [
    "Berita",
    "E-Aspirasi",
    "Donasi",
    "Info Beasiswa",
    "Lowongan",
    "Give Away",
    "Marketplace"
  ];
  List<Image> listContentMenuImage = [
    Image.asset(Drawable.ic_berita, width: 48),
    Image.asset(Drawable.ic_aspirasi, width: 48),
    Image.asset(Drawable.ic_donasi, width: 48),
    Image.asset(Drawable.ic_beasiswa, width: 48),
    Image.asset(Drawable.ic_loker, width: 48),
    Image.asset(Drawable.ic_give_away, width: 48),
    Image.asset(Drawable.ic_market, width: 48)
  ];

  List<dynamic> listNotification = [
    {
      'title': "Loremp ipsum color at amet",
      'description': "Loremp ipsum color at amet"
    },
    {
      'title': "Loremp ipsum color at amet",
      'description': "Loremp ipsum color at amet"
    }
  ];

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  Widget balanceWidget() {
    return GestureDetector(
        onTap: () {
          goto(Routes.topUp);
        },
        child: Row(children: [
          Expanded(
              child: ItemDrawLeftState(
                  title: Align(
                      child: Text(
                        "Saldo",
                        style: TextStyle(color: ColorStyle.white),
                      ),
                      alignment: Alignment.bottomLeft),
                  subtitle: Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        "Rp. 1.000.000",
                        style: TextStyle(color: ColorStyle.white, fontSize: 12),
                      )),
                  icon: Container(
                      margin: EdgeInsets.only(right: 4),
                      child: Image.asset(Drawable.ic_wallet, width: 36)))),
          Expanded(
              child: ItemDrawLeftState(
                  title: Align(
                      child: Text("Top Up Saldo",
                          style: TextStyle(color: ColorStyle.white)),
                      alignment: Alignment.bottomLeft),
                  icon: Container(
                      margin: EdgeInsets.only(right: 4),
                      child: Image.asset(Drawable.ic_top_up, width: 36))))
        ]));
  }

  Widget menuWidget() {
    return GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: listMenu.length,
        shrinkWrap: mounted,
        padding: EdgeInsets.only(top: 16),
        children: List.generate(listMenu.length, (position) {
          var item = listMenu[position];
          var itemImage = listMenuImage[position];
          var itemAction = listMenuAction[position];
          return GestureDetector(
              onTap: () {
                goto(itemAction);
              },
              child: Column(
                children: [
                  Container(
                      decoration: BoxDecoration(
                          color: ColorStyle.primary,
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: itemImage),
                  SizedBox(height: 4),
                  Text(item, style: TextStyle(color: ColorStyle.white))
                ],
              ));
        }));
  }

  Widget bottomNavigationBar(int currentIndex) {
    return ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        child: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.home), title: Text("Home")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.notifications), title: Text("Notifikasi")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person), title: Text("Profil"))
            ],
            currentIndex: currentIndex,
            selectedItemColor: ColorStyle.primary,
            unselectedItemColor: ColorStyle.white,
            backgroundColor: ColorStyle.brown,
            showSelectedLabels: true,
            type: BottomNavigationBarType.fixed,
            selectedFontSize: 12,
            unselectedFontSize: 12,
            showUnselectedLabels: true,
            elevation: 0.5,
            onTap: (value) {
              setState(() {
                currentBottomIndex = value;
              });
            }));
  }

  Widget contentMenuWidget() {
    return GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 4,
        shrinkWrap: mounted,
        padding: EdgeInsets.only(top: 16),
        children: List.generate(listContentMenu.length, (position) {
          var item = listContentMenu[position];
          var itemImage = listContentMenuImage[position];
          return Column(
            children: [
              itemImage,
              SizedBox(height: 4),
              Align(
                  child: Text(item,
                      style: TextStyle(color: ColorStyle.text),
                      textAlign: TextAlign.center),
                  alignment: Alignment.center)
            ],
          );
        }));
  }

  Widget fragmentHome() {
    return Column(children: [
      BoxUpperContentState(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(top: 48, left: 16, right: 16),
            child: Row(children: [
              Expanded(
                  child: Column(
                children: [
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Image.asset(Drawable.text_logo, width: 125),
                  ),
                  Image.asset(Drawable.text_tagline, width: 200),
                ],
              )),
              Image.asset(Drawable.face_logo, width: 125, height: 134),
            ]),
          ),
          WrapBrownState(
              child: Container(
            padding: EdgeInsets.only(top: 16, left: 16, right: 16),
            child: Column(
              children: [
                balanceWidget(),
                SizedBox(height: 16),
                Divider(color: ColorStyle.accent, height: 1),
                menuWidget()
              ],
            ),
          )),
          SizedBox(height: 16),
        ],
      )),
      Container(
          margin: EdgeInsets.all(16),
          child: Text(Values.adekuUpdate,
              style: TextStyle(
                  color: ColorStyle.text,
                  fontSize: 16,
                  fontWeight: FontWeight.bold))),
      contentMenuWidget()
    ]);
  }

  Widget fragmentNotification() {
    return Column(
      children: [
        BoxNavContentState(
            appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(Values.titleNotification,
                  textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
              alignment: Alignment.center),
        )),
        Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                padding: EdgeInsets.only(top: 8, bottom: 8, left: 4, right: 4),
                itemCount: listNotification.length,
                itemBuilder: (BuildContext contex, int index) {
                  var item = listNotification[index];
                  return Container(
                      margin:
                          EdgeInsets.only(top: 2, bottom: 2, left: 8, right: 8),
                      decoration: BoxDecoration(
                          color: ColorStyle.brown,
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: ListTile(
                        leading: Icon(Icons.message,
                            size: 48, color: ColorStyle.primary),
                        title: Text(
                          item['title'],
                          style: TextStyle(color: ColorStyle.primary),
                        ),
                        subtitle: Text(
                          item['description'],
                          style: TextStyle(color: ColorStyle.white),
                        ),
                      ));
                }))
      ],
    );
  }

  Widget profileWidget() {
    return Container(
        margin: EdgeInsets.only(top: 64),
        width: width,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Container(
            height: 80,
            width: 80,
            margin: EdgeInsets.only(top: 32),
            decoration: BoxDecoration(
                border: Border.all(width: 4, color: ColorStyle.primaryDark),
                color: ColorStyle.brown,
                borderRadius: BorderRadius.circular(360),
                image: DecorationImage(image: AssetImage(Drawable.face_logo))),
          ),
          SizedBox(
            height: 16,
          ),
          WrapBrownState(
              child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(16),
            child: Column(children: [
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Anonim",
                  style: TextStyle(
                      color: ColorStyle.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w500),
                ),
              ),
              SizedBox(
                height: 4,
              ),
              Align(
                alignment: Alignment.center,
                child: Text("082119189690",
                    style: TextStyle(
                      color: ColorStyle.white,
                    )),
              ),
              Align(
                alignment: Alignment.center,
                child: Text("anonim@gmail.com",
                    style: TextStyle(
                      color: ColorStyle.white,
                    )),
              ),
            ]),
          )),
          SizedBox(
            height: 16,
          ),
        ]));
  }

  Widget profileMenuWidget() {
    return Container(
      margin: EdgeInsets.only(top: 24, bottom: 24),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {},
            child: ItemActionState(
                title: Text("Kontak Kami",
                    style: TextStyle(color: ColorStyle.text)),
                icon: Icon(
                  Icons.chevron_right,
                  color: ColorStyle.text,
                )),
          ),
          GestureDetector(
            onTap: () {},
            child: ItemActionState(
                title: Text("Ketentuan Layanan",
                    style: TextStyle(color: ColorStyle.text)),
                icon: Icon(
                  Icons.chevron_right,
                  color: ColorStyle.text,
                )),
          ),
          GestureDetector(
            onTap: () {},
            child: ItemActionState(
                title: Text("Kebijakan Privasi",
                    style: TextStyle(color: ColorStyle.text)),
                icon: Icon(
                  Icons.chevron_right,
                  color: ColorStyle.text,
                )),
          ),
          GestureDetector(
            onTap: () {},
            child: ItemActionState(
                title:
                    Text("Bantuan", style: TextStyle(color: ColorStyle.text)),
                icon: Icon(
                  Icons.chevron_right,
                  color: ColorStyle.text,
                )),
          ),
          GestureDetector(
            onTap: () {},
            child: ItemActionState(
                title:
                    Text("Logout", style: TextStyle(color: ColorStyle.primary)),
                color: ColorStyle.brown,
                icon: Icon(
                  Icons.subdirectory_arrow_right,
                  color: ColorStyle.primary,
                )),
          )
        ],
      ),
    );
  }

  Widget fragmentProfile() {
    return Column(children: [
      BoxUpperContentState(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            title: Align(
                child: Text(Values.titleProfile,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18)),
                alignment: Alignment.center),
            actions: [
              IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: ColorStyle.brown,
                  ),
                  onPressed: () {})
            ],
          ),
          child: profileWidget()),
      profileMenuWidget()
    ]);
  }

  Widget fragmentManager() {
    return (currentBottomIndex == 0)
        ? SingleChildScrollView(child: fragmentHome())
        : (currentBottomIndex == 1)
            ? fragmentNotification()
            : SingleChildScrollView(child: fragmentProfile());
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: ColorStyle.white,
      body: fragmentManager(),
      bottomNavigationBar: bottomNavigationBar(currentBottomIndex),
    );
  }
}
