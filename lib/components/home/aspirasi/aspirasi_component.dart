import 'dart:convert';

import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:gson/gson.dart';

class AspirasiComponent extends StatefulWidget {
  @override
  State createState() => AspirasiComponentState();
}

class AspirasiComponentState extends State<AspirasiComponent> {
  var width, height;

  int currentBottomIndex = 0;
  TextEditingController controller;

  List<dynamic> listBerita = [];

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  @override
  void initState() {
    super.initState();

    controller = TextEditingController();

    loadAspirasi();
  }

  void loadAspirasi() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.easpirasi;
    body["page"] = 1;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        if (data["easpirasi"]["success"])
          setState(() {
            listBerita = data["easpirasi"]["result"];
          });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void doKirimAspirasi(String pesan) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.pesan] = pesan;
    await formData(Req.uriSendAspirasi, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        controller.clear();
        alertSuccess(context, data["message"]);
        loadAspirasi();
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  Widget topBar() {
    return BoxOrangeNavContentState(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(Values.titleAspirasi,
                  textAlign: TextAlign.start,
                  style:
                      TextStyle(fontSize: 18, color: ColorStyle.primaryDark)),
              alignment: Alignment.bottomLeft),
          iconTheme: IconThemeData(color: ColorStyle.primaryDark),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                  margin: EdgeInsets.only(top: 80, left: 32, right: 32),
                  alignment: Alignment.bottomRight,
                  child: Image.asset(
                    Drawable.face_logo,
                    width: 140,
                  )),
            ),
            Container(
              margin: EdgeInsets.only(top: 80, bottom: 16, left: 32, right: 32),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Image.asset(Drawable.ic_aspirasi),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Align(
                        alignment: Alignment.bottomLeft,
                        child: Text(
                          "Sampaikan Aspirasi Anda",
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold),
                        ))
                  ]),
            )
          ],
        ));
  }

  Widget beritaBar() {
    return ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 16, bottom: 16, left: 0, right: 0),
        itemCount: listBerita.length,
        itemBuilder: (BuildContext context, int position) {
          var item = listBerita[position];
          return GestureDetector(
              onTap: () {},
              child: Column(
                children: [
                  ListTile(
                    leading: Image.asset(Drawable.ic_berita),
                    title: Text(item["pesan"]),
                    subtitle: Text(item["updated"]),
                  ),
                  Divider(color: ColorStyle.gray, height: 1)
                ],
              ));
        });
  }

  void openBottomSheetDialog() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Wrap(children: [
            Container(
                padding: MediaQuery.of(context).viewInsets,
                color: ColorStyle.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                          padding: EdgeInsets.all(16),
                          child: TextAreaState(
                              controller: controller,
                              onChange: () {},
                              hint: "Pesan anda")),
                      FlatButton(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 0, bottom: 16),
                          onPressed: () {
                            Navigator.pop(context);
                            doKirimAspirasi(controller.text);
                          },
                          child: Container(
                            height: 56,
                            padding: EdgeInsets.all(16),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: ColorStyle.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(Values.buttonSend,
                                        style: TextStyle(
                                            color: ColorStyle.white))),
                                Icon(Icons.send,
                                    size: 24, color: ColorStyle.white)
                              ],
                            ),
                          ))
                    ]))
          ]);
        });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        backgroundColor: ColorStyle.white,
        body: SingleChildScrollView(
            child: Column(children: [topBar(), beritaBar()])),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FlatButton(
            onPressed: () {
              openBottomSheetDialog();
            },
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(360),
                  color: ColorStyle.primary),
              child: Icon(Icons.add, color: ColorStyle.white, size: 24),
              padding: EdgeInsets.all(16),
            )));
  }
}
