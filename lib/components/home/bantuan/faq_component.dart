import 'dart:convert';

import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'package:gson/gson.dart';

class FaqComponent extends StatefulWidget {
  @override
  State createState() => FaqComponentState();
}

class FaqComponentState extends State<FaqComponent> {
  var width, height;
  dynamic dataDetail;
  List<dynamic> items, statuss = [];

  @override
  void initState() {
    super.initState();

    loadFaq();
  }

  void loadFaq() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.faqs;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        if (data["faqs"]["success"]) {
          List<dynamic> temp = data["faqs"]["results"];
          temp.forEach((element) {
            setState(() {
              statuss.add(false);
            });
          });
          setState(() {
            items = temp;
            statuss[0] = true;
          });
        } else
          alertError(context, data["faqs"]["message"]);
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
          title: Align(
              child: Text("F.A.Q",
                  textAlign: TextAlign.start,
                  style: TextStyle(fontSize: 18, color: ColorStyle.white)),
              alignment: Alignment.bottomLeft),
          iconTheme: IconThemeData(color: ColorStyle.white),
        ),
        backgroundColor: ColorStyle.white,
        body: (items == null)
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                shrinkWrap: true,
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return ExpansionPanelList(
                    animationDuration: Duration(seconds: 1),
                    children: [
                      ExpansionPanel(
                          headerBuilder: (context, isExpanded) {
                            return Container(
                              padding: EdgeInsets.only(
                                  left: 12, top: 12, bottom: 12),
                              child: (isExpanded)
                                  ? Text(items[index]["title"],
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500))
                                  : Text(items[index]["title"]),
                            );
                          },
                          isExpanded: statuss[index],
                          body: Container(
                              child: HtmlView(
                                data: items[index]["content"],
                                scrollable: false,
                              ),
                              padding: EdgeInsets.all(12)))
                    ],
                    expansionCallback: (panelIndex, isExpanded) {
                      setState(() {
                        statuss[index] = !statuss[index];
                      });
                    },
                  );
                },
              ));
  }
}
