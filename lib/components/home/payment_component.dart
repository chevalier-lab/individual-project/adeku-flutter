import 'dart:convert';

import 'package:adekuflutter/components/stateLess/CustomProducts.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:gson/gson.dart';

class PaymentComponent extends StatefulWidget {
  @override
  State createState() => PaymentComponentState();
}

class PaymentComponentState extends State<PaymentComponent> {
  var width, height;
  bool isDoChangePIN = false, _isLoading = true;
  List<dynamic> _payments = [];

  @override
  void initState() {
    super.initState();
    loadPayment();
  }

  void gotoHome() {
    Application.router.navigateTo(context, Routes.home,
        clearStack: true, replace: true, transition: TransitionType.fadeIn);
  }

  void loadPayment() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    // body[KeyParams.requestsVouchersProduct] = content['id'];
    body[KeyParams.requests0] = ValueParams.payments;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State _balance
        if (data['payments']['success']) {
          setState(() {
            _payments = data["payments"]["results"];
            _isLoading = false;
          });
        } else {
          alertError(context, data['payments']["message"]);
        }
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void doBeliProduct(String id) async {
    var auth = await getAuth();
    var selectedProduct = await getSelectedProduct();
    var selectedPhone = await getSelectedPhone();
    var selectedPPOBCategory = await getSelectedPPOBCategory();
    var authData = gson.decode(auth);
    var productData = gson.decode(selectedProduct);
    var phoneData = gson.decode(selectedPhone);
    var ppobCategoryData = gson.decode(selectedPPOBCategory);

    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.quantity] = 1;
    body[KeyParams.phone] = phoneData;
    body[KeyParams.voucherId] = productData["id"];
    // body[KeyParams.requestsVouchersProduct] = content['id'];
    body[KeyParams.payment] = id;

    switch (ppobCategoryData['id']) {
      case CustomProductsAction.token_pln:
      case CustomProductsAction.tagihan:
        var selectedIDPelanggan = await getSelectedIDPelanggan();
        var idPlgn = gson.decode(selectedIDPelanggan);
        body[KeyParams.idPlgn] = idPlgn;
        break;
      case CustomProductsAction.donasi:
        var selectedDonation = await getSelectedDonation();
        var donation = gson.decode(selectedDonation);
        body[KeyParams.donation] = donation;
        body[KeyParams.comment] = "";
        body[KeyParams.anonymous] = 1;
        break;
    }

    await formData(Req.orderBuy, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State _balance
        if (data['status']) {
          gotoHome();
        } else {
          alertError(context, data["message"]);
        }
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ColorStyle.primary,
          title: Text(Values.titleMetodePembayaran,
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
        ),
        body: (_isLoading)
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                padding: EdgeInsets.only(top: 8, bottom: 8, left: 4, right: 4),
                itemCount: _payments.length,
                itemBuilder: (BuildContext contex, int index) {
                  var item = _payments[index];
                  return GestureDetector(
                    onTap: () {
                      doBeliProduct(item['id']);
                    },
                    child: Container(
                        margin: EdgeInsets.only(
                            top: 2, bottom: 2, left: 8, right: 8),
                        decoration: BoxDecoration(
                            color: ColorStyle.gray,
                            borderRadius: BorderRadius.all(Radius.circular(8))),
                        child: ListTile(
                          title: Text(item['name'],
                              style: TextStyle(color: ColorStyle.primaryDark)),
                          subtitle: Text(
                            "Status: ${(item['disabled']) ? 'Tidak Aktif' : 'Aktif'}",
                            style: TextStyle(color: ColorStyle.text),
                          ),
                          trailing: Icon(Icons.chevron_right,
                              size: 24, color: ColorStyle.grayDark),
                        )),
                  );
                }));
  }
}
