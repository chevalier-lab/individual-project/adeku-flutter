import 'dart:convert';
import 'dart:io';

import 'package:adekuflutter/components/home/bantuan/faq_component.dart';
import 'package:adekuflutter/components/stateLess/ContainerState.dart';
import 'package:adekuflutter/components/stateLess/CustomProducts.dart';
import 'package:adekuflutter/components/stateLess/FieldState.dart';
import 'package:adekuflutter/components/stateLess/ItemState.dart';
import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:dio/dio.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adekuflutter/configs/application.dart';
import 'package:adekuflutter/configs/routes.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:gson/gson.dart';
import 'package:image_picker/image_picker.dart';

class HomeComponent extends StatefulWidget {
  @override
  State createState() => HomeComponentState();
}

class HomeComponentState extends State<HomeComponent> {
  var width, height;

  int currentBottomIndex = 0, notificationStatus = 2, notificationPage = 1;
  dynamic _balance, _profile;
  int _notificationBadge = 0;
  List<dynamic> _notification, _categories = [], _transaction;

  TextEditingController contactUsController,
      fullNameController,
      addressController;

  String gender = "";

  File _image;
  final picker = ImagePicker();

  List<String> listMenu = ["Pulsa", "Internet", "Listrik", "BPJS"];
  List<Image> listMenuImage = [
    Image.asset(Drawable.ic_pulsa, width: 36),
    Image.asset(Drawable.ic_internet, width: 36),
    Image.asset(Drawable.ic_listrik, width: 36),
    Image.asset(Drawable.ic_bpjs, width: 36)
  ];
  List<String> listMenuAction = [
    Routes.pulsaNew,
    Routes.internetNew,
    Routes.listrikNew,
    Routes.bpjsNew
  ];

  List<String> listContentMenu = [
    "Berita",
    "E-Aspirasi",
    "Donasi",
    "Info Beasiswa",
    "Lowongan",
    "Give Away",
    "Marketplace"
  ];
  List<Image> listContentMenuImage = [
    Image.asset(Drawable.ic_berita, width: 48),
    Image.asset(Drawable.ic_aspirasi, width: 48),
    Image.asset(Drawable.ic_donasi, width: 48),
    Image.asset(Drawable.ic_beasiswa, width: 48),
    Image.asset(Drawable.ic_loker, width: 48),
    Image.asset(Drawable.ic_give_away, width: 48),
    Image.asset(Drawable.ic_market, width: 48)
  ];
  List<String> listContentMenuAction = [
    Routes.berita,
    Routes.aspirasi,
    Routes.donasi,
    Routes.infoBeasiswa,
    Routes.infoLoker,
    Routes.giveAway,
    Routes.market
  ];

  List<dynamic> listNotification = [
    {
      'title': "Loremp ipsum color at amet",
      'description': "Loremp ipsum color at amet"
    },
    {
      'title': "Loremp ipsum color at amet",
      'description': "Loremp ipsum color at amet"
    }
  ];

  @override
  void initState() {
    super.initState();

    contactUsController = TextEditingController();
    fullNameController = TextEditingController();
    addressController = TextEditingController();

    loadBalance();
    loadNotificationBadge();
    // loadValidators();
    loadCategories();
  }

  void goto(String route) {
    Application.router.navigateTo(context, route,
        clearStack: false, replace: false, transition: TransitionType.fadeIn);
  }

  void loadBalance() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.balance;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State _balance
        setState(() {
          _balance = data["results"];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadTransaction(String status) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requestsTransactionStatus] = status;
    body[KeyParams.requestsTransactionPage] = 1;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        setState(() {
          _transaction = data['transactions']['results'];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadNotification() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requestsNotificationsStatus] = notificationStatus;
    body[KeyParams.requestsNotificationsPage] = notificationPage;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State List Notification
        setState(() {
          _notification = data['notifications']['results'];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  Future openDetail(dynamic data) async {
    var status = await Navigator.of(context).push(new MaterialPageRoute<bool>(
        builder: (BuildContext context) {
          return data;
        },
        fullscreenDialog: true));
  }

  void loadNotificationBadge() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.unreadNotificationCount;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State List Notification
        setState(() {
          _notificationBadge = data['unread_notification_count']['result'];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void doReadNotification(int id) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.id] = id;
    await formData(Req.markAsReadNotification, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State List Notification
        loadNotificationBadge();
        loadNotification();
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadProfile() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.accountDetails;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
        setState(() {
          _profile = data['account_details']['results'];
          fullNameController.text = data['account_details']['results']["name"];
          addressController.text =
              data['account_details']['results']["address"];
          gender = data['account_details']['results']["gender"];
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadValidators() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.validators;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
        // setState(() {
        //   _profile = data['account']['results'];
        // });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadCategories() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.products;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
        List<dynamic> category = data['products']['results'];
        setSelectedPPOBCategoryDonation(category[category.length - 1]);
        category.removeLast();
        setState(() {
          _categories = category;
        });
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadProfileDetail() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.account;
    await formData(Req.login, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void doKirimFeedback(String feedback) async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.message] = feedback;
    await formData(Req.uriSendFeedback, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        alertSuccess(context, data["message"]);
        contactUsController.clear();
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void doKirimProfile() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.nama] = fullNameController.text;
    body[KeyParams.jenisKelamin] = (gender == "") ? "male" : gender;
    body[KeyParams.alamat] = addressController.text;
    await formData(Req.updateProfile, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        alertSuccess(context, data["message"]);
        loadProfile();
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadTOS() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.tos;
    await formData(Req.uriGet, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void loadCustomerCare() async {
    var auth = await getAuth();
    var authData = gson.decode(auth);
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = authData["token"];
    body[KeyParams.authUsername] = authData["username"];
    body[KeyParams.requests0] = ValueParams.customerCare;
    await formData(Req.customerCare, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        // Todo: Set State Profile Checker
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  Widget balanceWidget() {
    return GestureDetector(
        onTap: () {
          goto(Routes.topUp);
        },
        child: Row(children: [
          Expanded(
              child: ItemDrawLeftState(
                  title: Align(
                      child: Text(
                        "Saldo",
                        style: TextStyle(color: ColorStyle.white),
                      ),
                      alignment: Alignment.bottomLeft),
                  subtitle: Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        (_balance != null) ? _balance['balance_str'] : "Rp 0",
                        style: TextStyle(color: ColorStyle.white, fontSize: 12),
                      )),
                  icon: Container(
                      margin: EdgeInsets.only(right: 4),
                      child: Image.asset(Drawable.ic_wallet, width: 36)))),
          Expanded(
              child: ItemDrawLeftState(
                  title: Align(
                      child: Text("Top Up Saldo",
                          style: TextStyle(color: ColorStyle.white)),
                      alignment: Alignment.bottomLeft),
                  icon: Container(
                      margin: EdgeInsets.only(right: 4),
                      child: Image.asset(Drawable.ic_top_up, width: 36))))
        ]));
  }

  Future openVoucher(dynamic data) async {
    setSelectedPPOBCategory(data);
    var status = await Navigator.of(context).push(new MaterialPageRoute<bool>(
        builder: (BuildContext context) {
          return new CustomProducts(state: data['id'], content: data);
        },
        fullscreenDialog: true));
    if (status != null) {
      if (status) {
        loadBalance();
        loadNotificationBadge();
      }
    }
  }

  Widget menuWidget() {
    return GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 4,
        shrinkWrap: mounted,
        padding: EdgeInsets.only(top: 16),
        children: List.generate(_categories.length, (position) {
          var item = _categories[position];
          var names = item['name'].split(' ');
          var name = (names.length > 1) ? names[1] : names[0];
          return GestureDetector(
              onTap: () {
                openVoucher(item);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      decoration: BoxDecoration(
                          color: ColorStyle.primary,
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      child: Icon(
                        Icons.add_box,
                        size: 32,
                      )),
                  SizedBox(height: 4),
                  Text(
                    name,
                    style: TextStyle(color: ColorStyle.white),
                    textAlign: TextAlign.center,
                  )
                ],
              ));
        }));
  }

  Widget bottomNavigationBar(int currentIndex) {
    return ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        child: BottomNavigationBar(
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.home), title: Text("Home")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.list), title: Text("Transaksi")),
              BottomNavigationBarItem(
                  icon: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.notifications),
                      Container(
                        padding: EdgeInsets.all(4),
                        decoration: BoxDecoration(
                            color: ColorStyle.red,
                            borderRadius: BorderRadius.circular(4)),
                        child: Text(
                            (_notificationBadge != null)
                                ? "$_notificationBadge"
                                : "0",
                            style: TextStyle(color: ColorStyle.white)),
                      )
                    ],
                  ),
                  title: Text("Notifikasi")),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person), title: Text("Profil"))
            ],
            currentIndex: currentIndex,
            selectedItemColor: ColorStyle.primary,
            unselectedItemColor: ColorStyle.white,
            backgroundColor: ColorStyle.brown,
            showSelectedLabels: true,
            type: BottomNavigationBarType.fixed,
            selectedFontSize: 12,
            unselectedFontSize: 12,
            showUnselectedLabels: true,
            elevation: 0.5,
            onTap: (value) {
              loadNotificationBadge();
              setState(() {
                currentBottomIndex = value;
              });
              if (value == 0) {
                loadBalance();
                // loadValidators();
                loadCategories();
              } else if (value == 1)
                loadTransaction("ok");
              else if (value == 2)
                loadNotification();
              else {
                loadProfile();
              }
            }));
  }

  Widget contentMenuWidget() {
    return GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 4,
        shrinkWrap: mounted,
        padding: EdgeInsets.only(top: 16),
        children: List.generate(listContentMenu.length, (position) {
          var item = listContentMenu[position];
          var itemImage = listContentMenuImage[position];
          return GestureDetector(
              onTap: () {
                goto(listContentMenuAction[position]);
              },
              child: Column(
                children: [
                  itemImage,
                  SizedBox(height: 4),
                  Align(
                      child: Text(item,
                          style: TextStyle(color: ColorStyle.text),
                          textAlign: TextAlign.center),
                      alignment: Alignment.center)
                ],
              ));
        }));
  }

  Widget fragmentHome() {
    return Column(children: [
      BoxUpperContentState(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(top: 48, left: 16, right: 16),
            child: Row(children: [
              Expanded(
                  child: Column(
                children: [
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Image.asset(Drawable.text_logo, width: 125),
                  ),
                  Image.asset(Drawable.text_tagline, width: 200),
                ],
              )),
              Image.asset(Drawable.face_logo, width: 125, height: 134),
            ]),
          ),
          WrapBrownState(
              child: Container(
            padding: EdgeInsets.only(top: 16, left: 16, right: 16),
            child: Column(
              children: [
                balanceWidget(),
                SizedBox(height: 16),
                Divider(color: ColorStyle.accent, height: 1),
                menuWidget()
              ],
            ),
          )),
          SizedBox(height: 16),
        ],
      )),
      Container(
          margin: EdgeInsets.all(16),
          child: Text(Values.adekuUpdate,
              style: TextStyle(
                  color: ColorStyle.text,
                  fontSize: 16,
                  fontWeight: FontWeight.bold))),
      contentMenuWidget()
    ]);
  }

  Widget notificationContent() {
    return Expanded(
        child: (_notification == null)
            ? Center(
                child: CircularProgressIndicator(),
              )
            : (_notification.length == 0)
                ? Center(
                    child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.error, size: 64, color: ColorStyle.grayDark),
                      Text("Belum ada notifikasi")
                    ],
                  ))
                : ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    padding:
                        EdgeInsets.only(top: 8, bottom: 8, left: 4, right: 4),
                    itemCount: _notification.length,
                    itemBuilder: (BuildContext contex, int index) {
                      var item = _notification[index];
                      return GestureDetector(
                        onTap: () {
                          doReadNotification(item['id']);
                        },
                        child: Container(
                            margin: EdgeInsets.only(
                                top: 2, bottom: 2, left: 8, right: 8),
                            decoration: BoxDecoration(
                                color: ColorStyle.brown,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: ListTile(
                              leading: Icon(Icons.message,
                                  size: 48, color: ColorStyle.primary),
                              title: Text(
                                item['title'],
                                style: TextStyle(color: ColorStyle.primary),
                              ),
                              subtitle: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    (item['message'].length > 30)
                                        ? "${item['message'].substring(0, 30)}..."
                                        : item['message'],
                                    style: TextStyle(color: ColorStyle.white),
                                  ),
                                  Text(
                                    item['date'],
                                    style: TextStyle(color: ColorStyle.white),
                                  )
                                ],
                              ),
                            )),
                      );
                    }));
  }

  Widget transactionContent() {
    return Expanded(
        child: (_transaction == null)
            ? Center(
                child: CircularProgressIndicator(),
              )
            : (_transaction.length == 0)
                ? Center(
                    child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.error, size: 64, color: ColorStyle.grayDark),
                      Text("Belum ada transaction")
                    ],
                  ))
                : ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    padding:
                        EdgeInsets.only(top: 8, bottom: 8, left: 4, right: 4),
                    itemCount: _notification.length,
                    itemBuilder: (BuildContext contex, int index) {
                      var item = _transaction[index];
                      return GestureDetector(
                        onTap: () {},
                        child: Container(
                            margin: EdgeInsets.only(
                                top: 2, bottom: 2, left: 8, right: 8),
                            decoration: BoxDecoration(
                                color: ColorStyle.brown,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: ListTile(
                              leading: Icon(Icons.message,
                                  size: 48, color: ColorStyle.primary),
                              title: Text(
                                item['title'],
                                style: TextStyle(color: ColorStyle.primary),
                              ),
                              subtitle: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    (item['message'].length > 30)
                                        ? "${item['message'].substring(0, 30)}..."
                                        : item['message'],
                                    style: TextStyle(color: ColorStyle.white),
                                  ),
                                  Text(
                                    item['date'],
                                    style: TextStyle(color: ColorStyle.white),
                                  )
                                ],
                              ),
                            )),
                      );
                    }));
  }

  Widget fragmentTransaction() {
    return Column(
      children: [
        BoxNavContentState(
            appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(Values.titleTransaction,
                  textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
              alignment: Alignment.center),
        )),
        transactionContent()
      ],
    );
  }

  Widget fragmentNotification() {
    return Column(
      children: [
        BoxNavContentState(
            appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: Align(
              child: Text(Values.titleNotification,
                  textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
              alignment: Alignment.center),
        )),
        notificationContent()
      ],
    );
  }

  Future getImage(int type) async {
    final pickedFile = await picker.getImage(
        source: (type == 0) ? ImageSource.camera : ImageSource.gallery);

    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });

      var auth = await getAuth();
      var authData = gson.decode(auth);
      var body = Map<String, dynamic>();
      body[KeyParams.authToken] = authData["token"];
      body[KeyParams.authUsername] = authData["username"];
      body[KeyParams.requests0] = ValueParams.userImage;
      body["image"] = await MultipartFile.fromFile(_image.path,
          filename: _image.path.split('/').last);
      await formData(Req.uriGet, body).then((dynamic value) {
        print(value.toString());
        var data = jsonDecode(value.toString());
        if (data["success"]) {
          alertSuccess(context, "Berhasil mengupload foto profil");
          loadProfile();
        } else {
          alertError(context, data["message"]);
        }
      }).catchError((onError) {
        print(onError.toString());
        alertError(context, onError.toString());
      });
    } else {
      print('No image selected.');
    }
  }

  void openBottomSheetDialogPhoto() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Wrap(children: [
            Container(
                padding: MediaQuery.of(context).viewInsets,
                color: ColorStyle.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          getImage(0);
                        },
                        child: Container(
                          padding: EdgeInsets.all(16),
                          child: Row(children: [
                            Icon(Icons.camera_alt, color: ColorStyle.text),
                            Expanded(child: Text(" Kamera"))
                          ]),
                        ),
                      ),
                      Divider(color: ColorStyle.gray, height: 1),
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                            getImage(1);
                          },
                          child: Container(
                            padding: EdgeInsets.all(16),
                            child: Row(children: [
                              Icon(Icons.picture_in_picture,
                                  color: ColorStyle.text),
                              Expanded(child: Text(" Galeri"))
                            ]),
                          ))
                    ]))
          ]);
        });
  }

  Widget profileWidget() {
    return Container(
        margin: EdgeInsets.only(top: 64),
        width: width,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          GestureDetector(
              onTap: () {
                openBottomSheetDialogPhoto();
              },
              child: Container(
                height: 80,
                width: 80,
                margin: EdgeInsets.only(top: 32),
                decoration: BoxDecoration(
                    border: Border.all(width: 4, color: ColorStyle.primaryDark),
                    color: ColorStyle.white,
                    borderRadius: BorderRadius.circular(360),
                    image: DecorationImage(
                        image: (_profile == null)
                            ? AssetImage(Drawable.face_logo)
                            : NetworkImage(_profile['image_url']))),
                child: Icon(Icons.camera_alt, color: ColorStyle.brown),
              )),
          SizedBox(
            height: 16,
          ),
          WrapBrownState(
              child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(16),
            child: (_profile == null)
                ? Center(child: CircularProgressIndicator())
                : Column(children: [
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        (_profile != null) ? _profile['name'] : "",
                        style: TextStyle(
                            color: ColorStyle.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Text((_profile != null) ? _profile['phone'] : "",
                          style: TextStyle(
                            color: ColorStyle.white,
                          )),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Text((_profile != null) ? _profile['email'] : "",
                          style: TextStyle(
                            color: ColorStyle.white,
                          )),
                    ),
                  ]),
          )),
          SizedBox(
            height: 16,
          ),
        ]));
  }

  void openBottomSheetDialog() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Wrap(children: [
            Container(
                padding: MediaQuery.of(context).viewInsets,
                color: ColorStyle.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                          padding: EdgeInsets.all(16),
                          child: TextAreaState(
                              controller: contactUsController,
                              onChange: () {},
                              hint: "Pesan anda")),
                      FlatButton(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 0, bottom: 16),
                          onPressed: () {
                            Navigator.pop(context);
                            doKirimFeedback(contactUsController.text);
                          },
                          child: Container(
                            height: 56,
                            padding: EdgeInsets.all(16),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: ColorStyle.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(Values.buttonSend,
                                        style: TextStyle(
                                            color: ColorStyle.white))),
                                Icon(Icons.send,
                                    size: 24, color: ColorStyle.white)
                              ],
                            ),
                          ))
                    ]))
          ]);
        });
  }

  void openBottomSheetDialogProfile() {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Wrap(children: [
            Container(
                padding: MediaQuery.of(context).viewInsets,
                color: ColorStyle.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 16, bottom: 4),
                          child: Text("Nama Lengkap")),
                      Container(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 0, bottom: 16),
                          child: TextState(
                              controller: fullNameController,
                              onChange: () {},
                              hint: "Nama Lengkap")),
                      Container(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 0, bottom: 4),
                          child: Text("Jenis Kelamin")),
                      Container(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 0, bottom: 16),
                          child: Row(
                            children: [
                              Radio(
                                value: "male",
                                groupValue: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = "male";
                                  });
                                  Navigator.pop(context);
                                  openBottomSheetDialogProfile();
                                },
                              ),
                              Text(
                                'Laki-laki',
                                style: new TextStyle(fontSize: 16.0),
                              ),
                              Radio(
                                value: "female",
                                groupValue: gender,
                                onChanged: (value) {
                                  setState(() {
                                    gender = "female";
                                  });
                                  Navigator.pop(context);
                                  openBottomSheetDialogProfile();
                                },
                              ),
                              Text(
                                'Perempuan',
                                style: new TextStyle(fontSize: 16.0),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 0, bottom: 4),
                          child: Text("Alamat")),
                      Container(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 0, bottom: 16),
                          child: TextAreaState(
                              controller: addressController,
                              onChange: () {},
                              hint: "Alamat")),
                      FlatButton(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, top: 0, bottom: 16),
                          onPressed: () {
                            Navigator.pop(context);
                            doKirimProfile();
                          },
                          child: Container(
                            height: 56,
                            padding: EdgeInsets.all(16),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: ColorStyle.green,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8))),
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(Values.buttonSend,
                                        style: TextStyle(
                                            color: ColorStyle.white))),
                                Icon(Icons.send,
                                    size: 24, color: ColorStyle.white)
                              ],
                            ),
                          ))
                    ]))
          ]);
        });
  }

  Widget profileMenuWidget() {
    return Container(
      margin: EdgeInsets.only(top: 24, bottom: 24),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              openBottomSheetDialog();
            },
            child: ItemActionState(
                title: Text("Kontak Kami",
                    style: TextStyle(color: ColorStyle.text)),
                icon: Icon(
                  Icons.chevron_right,
                  color: ColorStyle.text,
                )),
          ),
          GestureDetector(
            onTap: () {
              loadTOS();
            },
            child: ItemActionState(
                title: Text("Ketentuan Layanan",
                    style: TextStyle(color: ColorStyle.text)),
                icon: Icon(
                  Icons.chevron_right,
                  color: ColorStyle.text,
                )),
          ),
          GestureDetector(
            onTap: () {
              openDetail(FaqComponent());
            },
            child: ItemActionState(
                title: Text("F.A.Q", style: TextStyle(color: ColorStyle.text)),
                icon: Icon(
                  Icons.chevron_right,
                  color: ColorStyle.text,
                )),
          ),
          GestureDetector(
            onTap: () {
              loadCustomerCare();
            },
            child: ItemActionState(
                title: Text("Customer Care",
                    style: TextStyle(color: ColorStyle.text)),
                icon: Icon(
                  Icons.chevron_right,
                  color: ColorStyle.text,
                )),
          ),
          GestureDetector(
            onTap: () {},
            child: ItemActionState(
                title:
                    Text("Logout", style: TextStyle(color: ColorStyle.primary)),
                color: ColorStyle.brown,
                icon: Icon(
                  Icons.subdirectory_arrow_right,
                  color: ColorStyle.primary,
                )),
          )
        ],
      ),
    );
  }

  Widget fragmentProfile() {
    return Column(children: [
      BoxUpperContentState(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
            title: Align(
                child: Text(Values.titleProfile,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18)),
                alignment: Alignment.center),
            actions: [
              IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: ColorStyle.white,
                  ),
                  onPressed: () {
                    openBottomSheetDialogProfile();
                  })
            ],
          ),
          child: profileWidget()),
      profileMenuWidget()
    ]);
  }

  Widget fragmentManager() {
    return (currentBottomIndex == 0)
        ? SingleChildScrollView(child: fragmentHome())
        : (currentBottomIndex == 1)
            ? fragmentTransaction()
            : (currentBottomIndex == 2)
                ? fragmentNotification()
                : SingleChildScrollView(child: fragmentProfile());
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: ColorStyle.white,
      body: fragmentManager(),
      bottomNavigationBar: bottomNavigationBar(currentBottomIndex),
    );
  }
}
