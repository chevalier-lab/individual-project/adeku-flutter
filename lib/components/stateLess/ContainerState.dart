import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:flutter/widgets.dart';

class BoxBrownState extends StatelessWidget {
  BoxBrownState({@required this.child, @required this.width});
  final Widget child;
  var width;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: this.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(16)),
            color: ColorStyle.brown),
        margin: EdgeInsets.only(right: 16, left: 16),
        child: this.child);
  }
}

class WrapBrownState extends StatelessWidget {
  WrapBrownState({@required this.child});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(16)),
            color: ColorStyle.brown),
        margin: EdgeInsets.only(right: 16, left: 16),
        child: this.child);
  }
}

class BoxUpperContentState extends StatelessWidget {
  BoxUpperContentState({this.appBar, @required this.child});
  final Widget child;
  final Widget appBar;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      Container(
          decoration: BoxDecoration(
              color: ColorStyle.primary,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(16),
                  bottomRight: Radius.circular(16))),
          width: width,
          child: Stack(children: [
            Container(
                alignment: Alignment.topRight,
                child: Image.asset(Drawable.ring, width: 130, height: 108)),
            Container(
                alignment: Alignment.bottomLeft,
                child: Image.asset(Drawable.ring_2, width: 185, height: 213)),
            Container(alignment: Alignment.center, child: this.child),
            (appBar != null)
                ? Container(alignment: Alignment.topCenter, child: this.appBar)
                : Container(),
          ]))
    ]);
  }
}

class BoxNavContentState extends StatelessWidget {
  BoxNavContentState({@required this.appBar});
  final Widget appBar;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      Container(
          decoration: BoxDecoration(
              color: ColorStyle.primary,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(16),
                  bottomRight: Radius.circular(16))),
          width: width,
          child: Stack(children: [
            Container(
                alignment: Alignment.topRight,
                child: Image.asset(Drawable.ring_3, width: 130)),
            Container(alignment: Alignment.center, child: this.appBar),
          ]))
    ]);
  }
}

class BoxBrownNavContentState extends StatelessWidget {
  BoxBrownNavContentState({@required this.appBar, @required this.child});
  final Widget child;
  final Widget appBar;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      Container(
          decoration: BoxDecoration(
              color: ColorStyle.brown,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(16),
                  bottomRight: Radius.circular(16))),
          width: width,
          child: Stack(children: [
            Container(
                alignment: Alignment.topRight,
                child: Image.asset(Drawable.ring_3, width: 130)),
            Container(alignment: Alignment.center, child: this.child),
            Container(alignment: Alignment.center, child: this.appBar),
          ]))
    ]);
  }
}

class BoxOrangeNavContentState extends StatelessWidget {
  BoxOrangeNavContentState({@required this.appBar, @required this.child});
  final Widget child;
  final Widget appBar;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      Container(
          decoration: BoxDecoration(
              color: ColorStyle.primary,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(16),
                  bottomRight: Radius.circular(16))),
          width: width,
          child: Stack(children: [
            Container(
                alignment: Alignment.topRight,
                child: Image.asset(Drawable.ring, width: 130)),
            Container(alignment: Alignment.center, child: this.child),
            Container(alignment: Alignment.center, child: this.appBar)
          ]))
    ]);
  }
}
