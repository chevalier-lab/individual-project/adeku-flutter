import 'package:adekuflutter/components/home/donasi/product_donasi.dart';
import 'package:adekuflutter/components/home/internet/product_internet.dart';
import 'package:adekuflutter/components/home/listrik/product_pln.dart';
import 'package:adekuflutter/components/home/pulsa/product_pulsa.dart';
import 'package:adekuflutter/components/home/tagihan/product_tagihan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomProductsAction {
  static const String pulsa = "pulsa";
  static const String paket_internet = "paket_internet";
  static const String token_pln = "token_pln";
  static const String tagihan = "tagihan";
  static const String donasi = "donasi";
}

class CustomProducts extends StatefulWidget {
  CustomProducts({@required this.state, this.content});
  final String state;
  final dynamic content;

  @override
  State<StatefulWidget> createState() {
    switch (this.state) {
      case CustomProductsAction.pulsa:
        return ProductPulsa();
      case CustomProductsAction.paket_internet:
        return ProductInternet();
      case CustomProductsAction.token_pln:
        return ProductPLN();
      case CustomProductsAction.tagihan:
        return ProductTagihan();
      default:
        return ProductDonasi();
    }
  }
}
