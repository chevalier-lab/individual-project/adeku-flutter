import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DialogAlert {
  DialogAlert(this.context, {@required this.content});
  final Widget content;
  final BuildContext context;

  void show() {
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        pageBuilder: (context, animation, secondaryAnimation) {
          return Container();
        },
        context: this.context,
        transitionBuilder: (context, a1, a2, widget) => Transform.scale(
            scale: a1.value,
            child: Opacity(
                opacity: a1.value, child: AlertDialog(content: content))));
  }
}

class DialogWebView {
  DialogWebView(this.context, {@required this.url});
  final String url;
  final BuildContext context;

  void show() {
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        pageBuilder: (context, animation, secondaryAnimation) {
          return Container();
        },
        context: this.context,
        transitionBuilder: (context, a1, a2, widget) => Transform.scale(
            scale: a1.value,
            child: Opacity(
                opacity: a1.value,
                child: AlertDialog(
                    content: WebView(
                  initialUrl: this.url,
                  javascriptMode: JavascriptMode.unrestricted,
                )))));
  }
}
