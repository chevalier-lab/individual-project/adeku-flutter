import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

// Phone Number Field
class PhoneNumberState extends StatelessWidget {
  PhoneNumberState({@required this.controller, @required this.onChange});

  final TextEditingController controller;
  final VoidCallback onChange;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
          hintText: "82xxxxxxx",
          filled: true,
          fillColor: ColorStyle.brownLight,
          hintStyle: TextStyle(color: ColorStyle.gray),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          )),
      onChanged: (value) => onChange,
    );
  }
}

// Number Field
class NumberState extends StatelessWidget {
  NumberState(
      {this.hint,
      this.focusNode,
      @required this.controller,
      @required this.onChange});

  final TextEditingController controller;
  final VoidCallback onChange;
  final String hint;
  final FocusNode focusNode;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      style: TextStyle(color: Colors.white),
      focusNode: (this.focusNode != null) ? this.focusNode : FocusNode(),
      decoration: InputDecoration(
          hintText: (hint != null) ? hint : "",
          filled: true,
          fillColor: ColorStyle.brownLight,
          hintStyle: TextStyle(color: ColorStyle.gray),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          )),
      onChanged: (value) => onChange,
    );
  }
}

// Email Field
class EmailState extends StatelessWidget {
  EmailState({@required this.controller, @required this.onChange});

  final TextEditingController controller;
  final VoidCallback onChange;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: TextInputType.emailAddress,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
          hintText: Values.labelEmail,
          filled: true,
          fillColor: ColorStyle.brownLight,
          hintStyle: TextStyle(color: ColorStyle.gray),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          )),
      onChanged: (value) => onChange,
    );
  }
}

// Text Field
class TextState extends StatelessWidget {
  TextState({this.hint, @required this.controller, @required this.onChange});

  final String hint;
  final TextEditingController controller;
  final VoidCallback onChange;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: TextInputType.text,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
          filled: true,
          fillColor: ColorStyle.brownLight,
          hintText: hint,
          hintStyle: TextStyle(color: ColorStyle.gray),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          )),
      onChanged: (value) => onChange,
    );
  }
}

// Text Area Field
class TextAreaState extends StatelessWidget {
  TextAreaState(
      {this.hint, @required this.controller, @required this.onChange});

  final String hint;
  final TextEditingController controller;
  final VoidCallback onChange;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      keyboardType: TextInputType.multiline,
      style: TextStyle(color: Colors.white),
      decoration: InputDecoration(
          filled: true,
          fillColor: ColorStyle.brownLight,
          hintText: hint,
          hintStyle: TextStyle(color: ColorStyle.gray),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.red),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: ColorStyle.brownLight),
          )),
      onChanged: (value) => onChange,
    );
  }
}

// Password Field
class PasswordState extends StatelessWidget {
  PasswordState(
      {@required this.controller,
      @required this.obsecure,
      @required this.onChange});

  final TextEditingController controller;
  final bool obsecure;
  final VoidCallback onChange;

  @override
  Widget build(BuildContext context) {
    return TextField(
        controller: controller,
        obscureText: obsecure,
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
            hintText: Values.labelPassword,
            filled: true,
            fillColor: ColorStyle.brownLight,
            hintStyle: TextStyle(color: ColorStyle.gray),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: ColorStyle.red),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: ColorStyle.brownLight),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: ColorStyle.brownLight),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: ColorStyle.brownLight),
            ),
            suffixIcon: GestureDetector(
                onTap: onChange,
                child: Icon(
                    (obsecure ? Icons.visibility_off : Icons.visibility),
                    color: ColorStyle.white))));
  }
}
