import 'package:adekuflutter/constants/images_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BackgroundState extends StatelessWidget {
  BackgroundState({this.appBar, @required this.child});
  final Widget child;
  final AppBar appBar;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return SingleChildScrollView(
        child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      Container(
          width: width,
          height: height,
          child: Stack(children: [
            Container(
                alignment: Alignment.topRight,
                child: Image.asset(Drawable.ring, width: 130, height: 108)),
            Container(
                alignment: Alignment.bottomLeft,
                child: Image.asset(Drawable.ring_2, width: 185, height: 213)),
            (appBar != null) ? appBar : Container(),
            Container(alignment: Alignment.center, child: this.child),
          ]))
    ]));
  }
}
