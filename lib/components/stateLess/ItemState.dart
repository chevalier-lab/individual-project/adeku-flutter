import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ItemDrawLeftState extends StatelessWidget {
  ItemDrawLeftState({@required this.title, this.subtitle, @required this.icon});
  Widget title, subtitle, icon;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        icon,
        Expanded(
            child: Column(
                children: [title, (subtitle != null) ? subtitle : Container()]))
      ],
    );
  }
}

class ItemActionState extends StatelessWidget {
  ItemActionState({@required this.title, this.color, @required this.icon});
  Widget title, icon;
  Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      margin: EdgeInsets.only(top: 4, bottom: 4, left: 16, right: 16),
      decoration: BoxDecoration(
          border: Border.all(color: ColorStyle.grayDark, width: 1),
          borderRadius: BorderRadius.circular(4),
          color: (color != null) ? color : ColorStyle.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [Expanded(child: title), icon],
      ),
    );
  }
}
