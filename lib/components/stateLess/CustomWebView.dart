import 'dart:async';
import 'dart:convert';

import 'package:adekuflutter/components/widgets/alert.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:adekuflutter/constants/images_constant.dart';
import 'package:adekuflutter/constants/values_constant.dart';
import 'package:adekuflutter/helpers/request_helpers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CustomWebViewAction {
  static const String termCondition = "term-condition";
  static const String detailDeposit = "detail-deposit";
  static const String verification = "verification";
  static const String pin = "pin";
  static const String otp = "otp";
}

class CustomWebView extends StatefulWidget {
  CustomWebView({@required this.state, @required this.url, this.content});
  final String state, url;
  final dynamic content;

  @override
  State<StatefulWidget> createState() {
    switch (this.state) {
      case CustomWebViewAction.otp:
        return WebViewOTP();
      case CustomWebViewAction.pin:
        return WebViewPIN();
      case CustomWebViewAction.verification:
        return WebViewVerification();
      case CustomWebViewAction.detailDeposit:
        return WebViewDetailDeposit();
      default:
        return WebViewTermAndCondition();
    }
  }
}

class WebViewDetailDeposit extends State<CustomWebView> {
  var width, height;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ColorStyle.primary,
          title: Text(Values.titleDetailDeposit,
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          ListTile(
            leading: Image.network(widget.content['payment_logo']),
            title: Text(widget.content['invoice_id']),
            subtitle: Text(widget.content['payment']),
          ),
          SizedBox(height: 32),
          Align(
              alignment: Alignment.center,
              child: Text("Total Tagihan",
                  style: TextStyle(
                      color: ColorStyle.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w500))),
          SizedBox(height: 8),
          Align(
              alignment: Alignment.center,
              child: Text(widget.content['amount_str'],
                  style: TextStyle(
                      color: ColorStyle.primary,
                      fontSize: 36,
                      fontWeight: FontWeight.w500))),
          SizedBox(height: 8),
          Align(
              alignment: Alignment.center,
              child: Text("Expire pada ${widget.content['expired_time']}",
                  style: TextStyle(color: ColorStyle.grayDark, fontSize: 14))),
          SizedBox(height: 32),
          Container(
            width: width,
            padding: EdgeInsets.all(16),
            margin: EdgeInsets.all(16),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: ColorStyle.accent,
                borderRadius: BorderRadius.circular(8)),
            child: Text(Values.getStatus(widget.content['status'])),
          )
        ]));
  }
}

class WebViewVerification extends State<CustomWebView> {
  var width, height;
  List<dynamic> verify = [
    {"item": "Nomor HP", "content": "", "status": false, "isOptional": false},
    {
      "item": "Alamat E-mail",
      "content": "",
      "status": false,
      "isOptional": false
    },
    {"item": "Nomor KTP", "content": "", "status": false, "isOptional": true}
  ];
  int selectedVerifyIndex = -1;
  dynamic selectedVerify;
  String currentText = "";
  TextEditingController verifyController;
  StreamController<ErrorAnimationType> errorController;
  bool hasError = false, isDoVerify = false, isLoadedVerify = false;

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
    verifyController = TextEditingController();
    loadVerifyList();
  }

  void loadVerifyList() async {
    print(widget.content.toString());
    var data = widget.content;
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = data["token"];
    body[KeyParams.authUsername] = data["username"];
    await formData(Req.verificationList, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
        if (data["results"]["verifications"]["phone"]["verified"] &&
            data["results"]["verifications"]["email"]["verified"]) {
          Navigator.of(context).pop(true);
        } else {
          setState(() {
            isLoadedVerify = true;
            verify[0]["content"] =
                data["results"]["verifications"]["phone"]["data"];
            verify[1]["content"] =
                data["results"]["verifications"]["email"]["data"];
            verify[2]["content"] =
                data["results"]["verifications"]["ktp"]["data"];

            verify[0]["status"] =
                data["results"]["verifications"]["phone"]["verified"];
            verify[1]["status"] =
                data["results"]["verifications"]["email"]["verified"];
            verify[2]["status"] =
                data["results"]["verifications"]["ktp"]["verified"];
          });
        }
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void action(dynamic item, int position) {
    if (!item["status"]) {
      setState(() {
        selectedVerify = item;
        selectedVerifyIndex = position;
      });
      switch (position) {
        case 0:
          doRequestPhoneNumber(item);
          break;
        case 1:
          doRequestEmail(item);
          break;
        default:
          break;
      }
    } else
      alertError(context, "${item['item']} sudah di verifikasi");
  }

  void doRequestEmail(dynamic item) async {
    var data = widget.content;
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = data["token"];
    body[KeyParams.authUsername] = data["username"];
    await formData(Req.verificationEmailReq, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void doRequestPhoneNumber(dynamic item) async {
    var data = widget.content;
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = data["token"];
    body[KeyParams.authUsername] = data["username"];
    await formData(Req.verificationPhoneReq, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      if (data["success"]) {
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void response() {
    switch (selectedVerifyIndex) {
      case 0:
        doResponsePhoneNumber();
        break;
      case 1:
        doResponseEmail();
        break;
      default:
    }
  }

  void doResponsePhoneNumber() async {
    var data = widget.content;
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = data["token"];
    body[KeyParams.authUsername] = data["username"];
    body[KeyParams.code] = currentText;
    await formData(Req.verificationPhoneRes, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      setState(() {
        isDoVerify = false;
      });
      if (data["success"]) {
        setState(() {
          selectedVerifyIndex = -1;
          verifyController.text = "";
          verifyController.dispose();
          verifyController = TextEditingController();
          errorController.close();
          errorController = StreamController<ErrorAnimationType>();
        });
        loadVerifyList();
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        isDoVerify = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  void doResponseEmail() async {
    var data = widget.content;
    var body = Map<String, dynamic>();
    body[KeyParams.authToken] = data["token"];
    body[KeyParams.authUsername] = data["username"];
    body[KeyParams.code] = currentText;
    await formData(Req.verificationEmailRes, body).then((dynamic value) {
      print(value.toString());
      var data = jsonDecode(value.toString());
      setState(() {
        isDoVerify = false;
      });
      if (data["success"]) {
        setState(() {
          selectedVerifyIndex = -1;
          verifyController.text = "";
          verifyController.dispose();
          verifyController = TextEditingController();
          errorController.close();
          errorController = StreamController<ErrorAnimationType>();
        });
        loadVerifyList();
      } else {
        alertError(context, data["message"]);
      }
    }).catchError((onError) {
      setState(() {
        isDoVerify = false;
      });
      print(onError.toString());
      alertError(context, onError.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ColorStyle.primary,
          title: Text(
              (!isLoadedVerify) ? "Sedang memuat.." : Values.titleVerify,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18)),
        ),
        body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: (!isLoadedVerify)
                  ? Center(child: CircularProgressIndicator())
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                          SizedBox(height: 16),
                          Align(
                              alignment: Alignment.center,
                              child: Image.asset(
                                  Drawable.undraw_forgot_password_gi2d,
                                  width: 205)),
                          SizedBox(height: 16),
                          contentVerify()
                        ]),
            )),
        bottomNavigationBar: buttonSave());
  }

  Widget formVerifyPhoneNumber() {
    return Column(
      children: [
        Padding(
            padding: EdgeInsets.only(left: 16, right: 16, top: 16),
            child: Align(
                alignment: Alignment.center,
                child: Text("Verifiasi Nomor HP",
                    style: TextStyle(fontSize: 16)))),
        verifyCodeField(),
        Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Align(
                alignment: Alignment.center,
                child: Text(
                    "Kami sudah mengirimkan Kode Verifikasi pada nomor anda",
                    textAlign: TextAlign.center)))
      ],
    );
  }

  Widget formVerifyEmail() {
    return Column(
      children: [
        Padding(
            padding: EdgeInsets.only(left: 16, right: 16, top: 16),
            child: Align(
                alignment: Alignment.center,
                child:
                    Text("Verifiasi E-Mail", style: TextStyle(fontSize: 16)))),
        verifyCodeField(),
        Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Align(
                alignment: Alignment.center,
                child: Text(
                    "Kami sudah mengirimkan Kode Verifikasi pada email anda",
                    textAlign: TextAlign.center)))
      ],
    );
  }

  Widget verifyCodeField() {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
        child: PinCodeTextField(
            length: 5,
            enableActiveFill: true,
            errorAnimationController: errorController,
            controller: verifyController,
            textInputType: TextInputType.number,
            pinTheme: PinTheme(
                shape: PinCodeFieldShape.box,
                fieldHeight: 60,
                fieldWidth: 60,
                inactiveFillColor: ColorStyle.white,
                activeFillColor: ColorStyle.white),
            animationDuration: Duration(milliseconds: 300),
            textStyle: TextStyle(fontSize: 20),
            backgroundColor: Colors.transparent,
            onChanged: (value) {
              print(value);
              setState(() {
                currentText = value;
              });
            }));
  }

  Widget contentVerify() {
    return Expanded(
        child: (selectedVerifyIndex == -1)
            ? ListView.builder(
                itemCount: verify.length,
                itemBuilder: (context, index) {
                  var item = verify[index];
                  return GestureDetector(
                    onTap: () {
                      action(item, index);
                    },
                    child: Column(
                      children: [
                        ListTile(
                            leading: (item["status"])
                                ? Icon(Icons.check,
                                    color: ColorStyle.green, size: 36)
                                : Icon(Icons.close,
                                    color: ColorStyle.red, size: 36),
                            title: Text(item["item"]),
                            subtitle: (item["isOptional"])
                                ? Text("Optional")
                                : Text("Wajib")),
                        Divider(color: ColorStyle.grayDark, height: 1)
                      ],
                    ),
                  );
                })
            : (selectedVerifyIndex == 0)
                ? formVerifyPhoneNumber()
                : formVerifyEmail());
  }

  Widget buttonSave() {
    return (selectedVerifyIndex == -1)
        ? Container(height: 56)
        : FlatButton(
            onPressed: () {
              if (!isDoVerify) {
                setState(() {
                  isDoVerify = true;
                });
                response();
              }
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              margin: EdgeInsets.only(top: 16, bottom: 16),
              padding: EdgeInsets.all(16),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ColorStyle.green,
                  borderRadius: BorderRadius.all(Radius.circular(16))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(Values.buttonSave),
                  (!isDoVerify)
                      ? Container()
                      : SizedBox(
                          width: 16,
                        ),
                  (!isDoVerify)
                      ? Container()
                      : SizedBox(
                          width: 24,
                          height: 24,
                          child: CircularProgressIndicator())
                ],
              ),
            ));
  }
}

class WebViewOTP extends State<CustomWebView> {
  var width, height;
  TextEditingController pinController;
  StreamController<ErrorAnimationType> errorController;
  bool hasError = false;
  String currentText = "";

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
    pinController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ColorStyle.primary,
          title: Text(Values.titleOTP,
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Image.asset(Drawable.undraw_forgot_password_gi2d, width: 205),
          SizedBox(height: 16),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
              child: PinCodeTextField(
                  length: 5,
                  enableActiveFill: true,
                  errorAnimationController: errorController,
                  controller: pinController,
                  textInputType: TextInputType.number,
                  pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      fieldHeight: 60,
                      fieldWidth: 60,
                      inactiveFillColor: ColorStyle.white,
                      activeFillColor: ColorStyle.white),
                  animationDuration: Duration(milliseconds: 300),
                  textStyle: TextStyle(fontSize: 20),
                  backgroundColor: Colors.transparent,
                  onChanged: (value) {
                    print(value);
                    setState(() {
                      currentText = value;
                    });
                  })),
          Padding(
              padding: EdgeInsets.only(left: 16, right: 16),
              child: Align(
                  alignment: Alignment.center,
                  child: Text(
                      "Kami sudah mengirimkan SMS OTP pada nomor ${widget.content['otp_value']}",
                      textAlign: TextAlign.center)))
        ]),
        bottomNavigationBar: FlatButton(
            onPressed: () {
              Navigator.of(context).pop(currentText);
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              margin: EdgeInsets.only(top: 16, bottom: 16),
              padding: EdgeInsets.all(16),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ColorStyle.green,
                  borderRadius: BorderRadius.all(Radius.circular(16))),
              child: Text(
                Values.buttonSave,
                style: TextStyle(color: ColorStyle.white),
              ),
            )));
  }
}

class WebViewPIN extends State<CustomWebView> {
  var width, height;
  TextEditingController pinController;
  StreamController<ErrorAnimationType> errorController;
  bool hasError = false;
  String currentText = "";

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
    pinController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;

    return new Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: ColorStyle.primary,
          title: Text(Values.titlePIN,
              textAlign: TextAlign.center, style: TextStyle(fontSize: 18)),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Image.asset(Drawable.undraw_forgot_password_gi2d, width: 205),
          SizedBox(height: 16),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
              child: PinCodeTextField(
                  length: 4,
                  enableActiveFill: true,
                  errorAnimationController: errorController,
                  controller: pinController,
                  textInputType: TextInputType.number,
                  pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      fieldHeight: 60,
                      fieldWidth: 60,
                      inactiveFillColor: ColorStyle.white,
                      activeFillColor: ColorStyle.white),
                  animationDuration: Duration(milliseconds: 300),
                  textStyle: TextStyle(fontSize: 20),
                  backgroundColor: Colors.transparent,
                  onChanged: (value) {
                    print(value);
                    setState(() {
                      currentText = value;
                    });
                  })),
        ]),
        bottomNavigationBar: FlatButton(
            onPressed: () {
              Navigator.of(context).pop(currentText);
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 56,
              margin: EdgeInsets.only(top: 16, bottom: 16),
              padding: EdgeInsets.all(16),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ColorStyle.green,
                  borderRadius: BorderRadius.all(Radius.circular(16))),
              child: Text(
                Values.buttonSave,
                style: TextStyle(color: ColorStyle.white),
              ),
            )));
  }
}

class WebViewTermAndCondition extends State<CustomWebView> {
  int index = 1;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('Syarat dan Ketentuan'),
      ),
      body: IndexedStack(index: index, children: [
        WebView(
            initialUrl: widget.url,
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (url) {
              setState(() {
                index = 0;
              });
            }),
        Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            )),
      ]),
      bottomNavigationBar: FlatButton(
          onPressed: () {
            Navigator.of(context).pop(true);
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 56,
            margin: EdgeInsets.only(top: 16, bottom: 16),
            padding: EdgeInsets.all(16),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                color: ColorStyle.green,
                borderRadius: BorderRadius.all(Radius.circular(16))),
            child: Text(
              Values.buttonAggree,
              style: TextStyle(color: ColorStyle.white),
            ),
          )),
    );
  }
}
