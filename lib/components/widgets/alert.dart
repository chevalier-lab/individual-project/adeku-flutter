import 'package:adekuflutter/components/stateLess/DialogState.dart';
import 'package:adekuflutter/constants/color_style_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void alertError(BuildContext context, String message) {
  DialogAlert(context,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Center(child: Icon(Icons.close, color: ColorStyle.red, size: 64)),
          SizedBox(
            height: 16,
          ),
          Align(alignment: Alignment.center, child: Text(message))
        ],
      )).show();
}

void alertSuccess(BuildContext context, String message) {
  DialogAlert(context,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Center(child: Icon(Icons.check, color: ColorStyle.green, size: 64)),
          SizedBox(
            height: 16,
          ),
          Align(alignment: Alignment.center, child: Text(message))
        ],
      )).show();
}
