class Drawable {
  static const uri = "assets/images/drawable/";

  // Drawable
  static const ring = uri + "ring_orange.png";
  static const ring_2 = uri + "ring2_orange.png";
  static const ring_3 = uri + "ring3_orange.png";

  static const face_logo = uri + "face_logo.png";
  static const text_logo = uri + "text_logo.png";
  static const text_logo_description = uri + "text_logo_description.png";
  static const text_tagline = uri + "text_tagline.png";

  static const top_up = uri + "top_up.png";

  static const undraw_forgot_password_gi2d =
      uri + "undraw_forgot_password_gi2d.png";

  static const undraw_my_password_d6kg = uri + "undraw_my_password_d6kg.png";
  static const undraw_transfer_money_rywa =
      uri + "undraw_transfer_money_rywa.png";

  static const ic_aspirasi = uri + "ic_aspirasi.png";
  static const ic_beasiswa = uri + "ic_beasiswa.png";
  static const ic_berita = uri + "ic_berita.png";
  static const ic_bpjs = uri + "ic_bpjs.png";
  static const ic_donasi = uri + "ic_donasi.png";
  static const ic_give_away = uri + "ic_give_away.png";
  static const ic_internet = uri + "ic_internet.png";
  static const ic_listrik = uri + "ic_listrik.png";
  static const ic_loker = uri + "ic_loker.png";
  static const ic_market = uri + "ic_market.png";
  static const ic_pulsa = uri + "ic_pulsa.png";
  static const ic_top_up = uri + "ic_top_up.png";
  static const ic_wallet = uri + "ic_wallet.png";

  static const ic_gopay = uri + "ic_gopay.png";
  static const ic_atm = uri + "ic_atm.png";
  static const ic_kaspro = uri + "ic_kaspro.png";
}
