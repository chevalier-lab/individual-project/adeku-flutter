import 'package:flutter/material.dart';

class ColorStyle {
  static final Color primary = Color(0xFFFE5000);
  static final Color primaryDark = Color(0xFFFF3608);
  static final Color accent = Color(0xFFFFE810);
  static final Color accentLight = Color(0xFFFCEFE7);
  static final Color white = Color(0xFFFFFFFF);
  static final Color black = Color(0xFF000000);
  static final Color green = Color(0xFF388e3c);
  static final Color red = Color(0xFFd32f2f);
  static final Color redLight = Color(0xFFFF7E84);
  static final Color gray = Color(0xFFEFEFEF);
  static final Color grayLight = Color(0xFFFBFBFB);
  static final Color grayDark = Color(0xFF8E8E8E);
  static final Color text = Color(0xFF353535);
  static final Color brown = Color(0xFF4C2D2A);
  static final Color brownLight = Color(0xFF534442);
}
