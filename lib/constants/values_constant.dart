class Values {
  // Title
  static final String titleLogin = "Masuk";
  static final String titleRegistration = "Daftar Akun";
  static final String titleNotification = "Notifikasi";
  static final String titleTransaction = "Transaksi";
  static final String titleTopUp = "Top Up";
  static final String titleProfile = "Profil";
  static final String titlePulsa = "Pulsa";
  static final String titleBerita = "Berita";
  static final String titleAspirasi = "Aspirasi";
  static final String titleDonasi = "Donasi";
  static final String titleBeasiswa = "Beasiswa";
  static final String titleLoker = "Lowongan Kerja";
  static final String titleGiveaway = "Giveaway";
  static final String titleMarket = "Market Place";
  static final String titleBPJS = "BPJS";
  static final String titleInternet = "Internet";
  static final String titleListrik = "Listrik";
  static final String titleNewProfile = "Buat Profil";
  static final String titleOTP = "Verifikasi OTP";
  static final String titleDetailDeposit = "Detail Deposit";
  static final String titleLostPassword = "Lost Password";
  static final String titlePIN = "Ubah PIN";
  static final String titleMetodePembayaran = "Metode Pembayaran";
  static final String titleVerify = "Verifikasi Akun";
  static final String orderBus = "Order Bus";
  static final String dataPenumpang = "Informasi Penumpang";
  static final String adekuUpdate = "Adeku Update";

  // Label
  static final String labelPhoneNumber = "Nomor Telepon";
  static final String labelFullName = "Nama Lengkap";
  static final String labelFirstName = "Nama Depan";
  static final String labelLastName = "Nama Belakang";
  static final String labelPassengerName = "Nama Penumpang";
  static final String labelEmail = "Email";
  static final String labelPassword = "Password";
  static final String labelDompetJak = "Dompet Jak";
  static final String labelCashOut = "Cash Out";
  static final String labelTopUp = "Top Up";
  static final String labelOrderBus = "Order Bus";
  static final String labelInfoJadwal = "Info Jadwal";
  static final String labelKeberangkatan = "Keberangkatan";
  static final String labelTujuan = "Tujuan";
  static final String labelTanggalKeberangkatan = "Tanggal Berangkat";
  static final String labelPulangPergi = "Pulang Pergi?";
  static final String labelJumlahKursi = "Jumlah Kursi";
  static final String labelJenisKelamin = "Jenis Kelamin";
  static final String labelPencarianTerakhir = "Pencarian Terakhir Anda";
  static final String labelPilihKursi = "Pilih Kursi";
  static final String labelFasilitas = "Fasilitas";
  static final String labelLostPassword = "Lost Password?";

  // Hint
  static final String hintCode = "+62";
  static final String hintPhoneNumber = "82xxxxxxxx";

  // Example
  static final String examplePhoneNumber = "contoh: 82119189690";
  static final String exampleName = "*Nama sesuaikan dengan KTP";

  // Description
  static final String descriptionTopUp =
      "ada beberapa cara untuk isi ulang saldo anda. Silahkan pilih salah satu Layanan yang tersedia berikut ini.";
  static final String descriptionNotHaveAccount = "Belum punya akun? ";
  static final String descriptionResetPassword =
      "Masukan Email Anda untuk menerima kode reset ulang kata sandi";
  static final String descriptionInsertPIN = "Masukan PIN Anda untuk keamanan";
  static final String descriptionRegistration =
      "Mohon masukkan nomor telepon anda, kami akan kirimkan nomor verifikasi melalui SMS";
  static final String descriptionNewProfil =
      "Mohon isi biodata dibawah ini\nuntuk keperluan pembuatan profil anda.";
  static final String descriptionTermsAndCondition =
      "Dengan ini saya telah menyetujui syarat dan ketentuan yang berlaku.";
  static final String descriptionOTP =
      "Kode verifikasi telah dikirimkan ke nomor anda, apabila belum menerima kode verifikasi sampai waktu habis, silahkan meminta kode baru";

  static final String descriptionTermsAndConditionAgree =
      "Mohon mengisi syarat dan ketentuan kami";

  // Button
  static final String buttonNext = "Selanjutnya";
  static final String buttonLogin = "Login";
  static final String buttonRegistration = "Daftar";
  static final String buttonNewRegistration = "Daftar Baru";
  static final String buttonChange = "Ubah";
  static final String buttonOrder = "Order Bus";
  static final String buttonCheckJadwal = "Cek Jadwal";
  static final String buttonSendCode = "Kirim Kode";
  static final String buttonSend = "Kirim";
  static final String buttonSave = "Simpan";
  static final String buttonCheckout = "Checkout";
  static final String buttonAggree = "Saya Setuju";

  static String getRouteMinify(String words) {
    var arr = words.split(" ");
    return (arr.length > 1) ? arr[0] + arr[1].substring(0, 3) : arr[0];
  }

  static String getStatus(String old) {
    switch (old) {
      case "WP":
        return "Menunggu Pembayaran";
      case "IP":
        return "Sedang Kami Proses";
      case "OK":
        return "Transaksi Berhasil";
      case "CL":
        return "Transaksi Dibatalkan";
      default:
        return "Refund";
    }
  }
}
