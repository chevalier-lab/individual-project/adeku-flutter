import 'package:shared_preferences/shared_preferences.dart';

class CacheHelpers {
  save(String label, String data) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString(label, data);
  }

  fetch(String label) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(label);
  }
}
