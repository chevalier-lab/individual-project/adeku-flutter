import 'package:dio/dio.dart';
import 'package:gson/gson.dart';
import 'package:adekuflutter/helpers/cache_helpers.dart';

class Req {
  static final String uri = "https://app.adeku.id/api/v2/";

  // URI GET
  static final String uriGet = "get";
  static final String uriSendAspirasi = "send-easpirasi";
  static final String uriSendFeedback = "send-feedback";

  // USER
  // Login
  static final String login = "login";
  static final String loginNew = "login_new";
  static final String loginOTP = "login_otp_phone";
  // Register
  static final String register = "register";
  static final String registerNew = "register_new";
  static final String registerTerms = "register-terms";
  // Pin
  static final String pin = "change-pin";
  // Forgot Password
  static final String forgotPassword = "forgot-password";
  // Verification
  static final String verificationList = "my_verification";
  static final String verificationPhoneReq = "verification/request/phone";
  static final String verificationPhoneRes = "verification/send/phone";
  static final String verificationEmailReq = "verification/request/email";
  static final String verificationEmailRes = "verification/send/email";

  // PROFILE
  // Update Profile
  static final String updateProfile = "update_profile";

  // TRANSACTION
  // Cancel Transaction
  static final String cancelTransaction = "cancel_transaction";

  // NOTIFICATION
  // Mark As Read
  static final String markAsReadNotification = "notifications-mark-as-read";

  // DEPOSIT
  // Cancel Deposit
  static final String depositCancel = "cancel_deposit";
  // Add Deposit
  static final String depositAdd = "deposit";

  // BANTUAN
  // Feedback
  static final String feedback = "send-feedback";
  // Customer Care
  static final String customerCare = "send-customer_care";

  // ORDER
  // Buy Order
  static final String orderBuy = "order";
}

class KeyParams {
  // Key
  static final String username = "username";
  static final String password = "password";
  static final String otp = "otp";
  static final String phone = "phone";
  static final String email = "email";
  static final String latitude = "latitude";
  static final String longitude = "longitude";
  static final String fullName = "full_name";
  static final String authToken = "auth_token";
  static final String authUsername = "auth_username";
  static final String pinSms = "pin_sms";
  static final String user = "user";
  static final String code = "code";
  static final String pesan = "pesan";
  static final String requests0 = "requests[0]";
  static final String nama = "nama";
  static final String jenisKelamin = "jenis_kelamin";
  static final String alamat = "alamat";
  static final String appRegId = "app_reg_id";
  static final String appVersionCode = "app_version_code";
  static final String appVersionName = "app_version_name";
  static final String image = "image";
  static final String requestsTransactionStatus =
      "requests[transactions][status]";
  static final String requestsTransactionPage = "requests[transactions][page]";
  static final String requestsTransactionDetailsId =
      "requests[transaction_details][id]";
  static final String id = "id";
  static final String requestsNotificationsStatus =
      "requests[notifications][status]";
  static final String requestsNotificationsPage =
      "requests[notifications][page]";
  static final String payment = "payment";
  static final String amount = "amount";
  static final String requestsDepositHistoryPage =
      "requests[deposit_history][page]";
  static final String requestsDepositDetailsId =
      "requests[deposit_details][id]";
  static final String requestsBalanceHistoryPage =
      "requests[balance_history][page]";
  static final String requestsBalanceHistoryStatus =
      "requests[balance_history][status]";
  static final String message = "message";
  static final String requestsVouchersProduct = "requests[vouchers][product]";
  static final String category = "category";
  static final String voucherId = "voucher_id";
  static final String subcategoryId = "subcategory_id";
  static final String requestsProductDetailsId =
      "requests[product_details][id]";
  static final String quantity = "quantity";
  static final String idPlgn = "id_plgn";
  static final String idVoucher = "id_voucher";
  static final String donation = "donation";
  static final String anonymous = "anonymous";
  static final String comment = "comment";
}

class ValueParams {
  // Value
  static final String balance = "balance";
  static final String berita = "berita";
  static final String beasiswa = "beasiswa";
  static final String lowongan = "lowongan";
  static final String giveaway = "giveaway";
  static final String easpirasi = "easpirasi";
  static final String beritaDetail = "berita_detail";
  static final String beasiswaDetail = "beasiswa_detail";
  static final String lowonganDetail = "lowongan_detail";
  static final String giveawayDetail = "giveaway_detail";
  static final String account = "account";
  static final String accountDetails = "account_details";
  static final String male = "male";
  static final String female = "female";
  static final String appRegId = "";
  static final String appVersionCode = "";
  static final String appVersionName = "";
  static final String userImage = "user_image";
  static final String ok = "ok";
  static final String ip = "ip";
  static final String cl = "cl";
  static final String unreadNotificationCount = "unread_notification_count";
  static final String faqs = "faqs";
  static final String tos = "tos";
  static final String customerCare = "customer_care";
  static final String payments = "payments";
  static final String validators = "validators";
  static final String products = "products";
  static final String categoryByPrefix = "category_by_prefix";
  static final String productByPrefix = "product_by_prefix";
  static final String subcategory = "subcategory";
  static final String productBySubcategory = "product_by_subcategory";
  static final String cekPin = "cek_pin";
  static final String cekIdPlgn = "cek_id_plgn";
  static final String cekTagihan = "cek_tagihan";
}

dynamic getIsPINChanged() async {
  var cache = CacheHelpers();
  var auth = await cache.fetch("change-pin");
  return auth;
}

dynamic getSelectedProduct() async {
  var cache = CacheHelpers();
  var auth = await cache.fetch("selected-product");
  return auth;
}

dynamic getSelectedPhone() async {
  var cache = CacheHelpers();
  var auth = await cache.fetch("selected-phone");
  return auth;
}

dynamic getSelectedDonation() async {
  var cache = CacheHelpers();
  var auth = await cache.fetch("selected-donation");
  return auth;
}

dynamic getSelectedIDPelanggan() async {
  var cache = CacheHelpers();
  var auth = await cache.fetch("selected-id-plgn");
  return auth;
}

dynamic getAuth() async {
  var cache = CacheHelpers();
  var auth = await cache.fetch("auth");
  return auth;
}

dynamic getTempAuth() async {
  var cache = CacheHelpers();
  var auth = await cache.fetch("authTemp");
  return auth;
}

dynamic getSelectedPPOBCategory() async {
  var cache = CacheHelpers();
  var auth = await cache.fetch("selected-ppob-category");
  return auth;
}

dynamic getSelectedPPOBCategoryDonation() async {
  var cache = CacheHelpers();
  var auth = await cache.fetch("selected-ppob-category-donation");
  return auth;
}

void setPINChange(dynamic data) {
  var cache = CacheHelpers();
  cache.save("change-pin", gson.encode(data));
}

void setSelectedProduct(dynamic data) {
  var cache = CacheHelpers();
  cache.save("selected-product", gson.encode(data));
}

void setSelectedPhone(dynamic data) {
  var cache = CacheHelpers();
  cache.save("selected-phone", gson.encode(data));
}

void setSelectedDonation(dynamic data) {
  var cache = CacheHelpers();
  cache.save("selected-donation", gson.encode(data));
}

void setSelectedPPOBCategory(dynamic data) {
  var cache = CacheHelpers();
  cache.save("selected-ppob-category", gson.encode(data));
}

void setSelectedPPOBCategoryDonation(dynamic data) {
  var cache = CacheHelpers();
  cache.save("selected-ppob-category-donation", gson.encode(data));
}

void setSelectedIDPelanggan(dynamic data) {
  var cache = CacheHelpers();
  cache.save("selected-id-plgn", gson.encode(data));
}

void setAuth(dynamic data) {
  var cache = CacheHelpers();
  cache.save("auth", gson.encode(data));
}

void setTempAuth(dynamic data) {
  var cache = CacheHelpers();
  cache.save("authTemp", gson.encode(data));
}

// Setting Dio
BaseOptions options = new BaseOptions(
  baseUrl: Req.uri,
  connectTimeout: 0,
  receiveTimeout: 0,
);
Dio dio = new Dio(options);

// Post Form Data
Future<Response> formData(String path, Map<String, dynamic> body) async =>
    await dio.post(path, data: FormData.fromMap(body));

// Post Form URL Encoded
Future<Response> formURLEncoded(String path, Map<String, dynamic> body) async =>
    await dio.post(path,
        data: body,
        options: Options(contentType: Headers.formUrlEncodedContentType));

// Post Form URL Encoded
Future<Response> raw(String path, String body) async =>
    await dio.post(path, data: body);
