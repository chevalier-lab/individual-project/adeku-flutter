import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'components/app/app_component.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initializeDateFormatting('id_ID', null)
      .then((_) => runApp(AppComponent()));
}
