import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import './route_handlers.dart';

class Routes {
  // Authentication
  static String root = "/";
  static String signIn = "/auth/signIn";
  static String signUp = "/auth/signUp";
  static String lostPassword = "/auth/lostPassword";

  static String pin = "/pin";

  static String home = "/home";

  static String payment = "/payment";

  static String topUp = "/home/topUp";
  static String topUpHistory = "/home/topUpHistory";
  static String topUpNew = "/home/topUp/:new";

  static String pulsa = "/home/pulsa";
  static String pulsaNew = "/home/pulsa/new";

  static String internet = "/home/internet";
  static String internetNew = "/home/internet/new";

  static String listrik = "/home/listrik";
  static String listrikNew = "/home/listrik/new";

  static String bpjs = "/home/bpjs";
  static String bpjsNew = "/home/bpjs/new";

  static String berita = "/home/berita";
  static String beritaDetail = "/home/berita/detail";

  static String aspirasi = "/home/aspirasi";
  static String aspirasiSuccess = "/home/aspirasi/success";

  static String donasi = "/home/donasi";

  static String infoBeasiswa = "/home/infoBeasiswa";

  static String infoLoker = "/home/infoLoker";

  static String giveAway = "/home/giveAway";

  static String market = "/home/market";

  static String notification = "/notification";

  static String profile = "/profile";
  static String profileEdit = "/profile/edit";

  static void configure(FluroRouter router) {
    router.notFoundHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      print("ROUTE WAS NOT FOUND !!!");
    });

    // Define Routing Auth
    router.define(root,
        handler: rootHandler, transitionType: TransitionType.fadeIn);
    router.define(signIn,
        handler: signInHandler, transitionType: TransitionType.fadeIn);
    router.define(signUp,
        handler: signUpHandler, transitionType: TransitionType.fadeIn);
    router.define(lostPassword,
        handler: lostPasswordHandler, transitionType: TransitionType.fadeIn);

    // Define Routing Home
    router.define(pin,
        handler: pinHandler, transitionType: TransitionType.fadeIn);
    router.define(payment,
        handler: paymentHandler, transitionType: TransitionType.fadeIn);

    // Define Routing Home
    router.define(home,
        handler: homeHandler, transitionType: TransitionType.fadeIn);
    // Define Routing Home Top Up
    router.define(topUp,
        handler: topUpHandler, transitionType: TransitionType.fadeIn);
    router.define(topUpHistory,
        handler: topUpHistoryHandler, transitionType: TransitionType.fadeIn);
    router.define(topUpNew,
        handler: topUpNewHandler, transitionType: TransitionType.fadeIn);
    // Define Routing Home Pulsa
    router.define(pulsaNew,
        handler: pulsaNewHandler, transitionType: TransitionType.fadeIn);
    // Define Routing Home Internet
    router.define(internetNew,
        handler: internetNewHandler, transitionType: TransitionType.fadeIn);
    // Define Routing Home Listrik
    router.define(listrikNew,
        handler: listrikNewHandler, transitionType: TransitionType.fadeIn);
    // Define Routing Home BPJS
    router.define(bpjsNew,
        handler: bpjsNewHandler, transitionType: TransitionType.fadeIn);

    // Define Routing Home Berita
    router.define(berita,
        handler: beritaHandler, transitionType: TransitionType.fadeIn);
    router.define(beritaDetail,
        handler: beritaDetailHandler, transitionType: TransitionType.fadeIn);

    // Define Routing Home Aspirasi
    router.define(aspirasi,
        handler: aspirasiHandler, transitionType: TransitionType.fadeIn);
    router.define(aspirasiSuccess,
        handler: aspirasiSuccessHandler, transitionType: TransitionType.fadeIn);

    // Define Routing Home Donasi
    router.define(donasi,
        handler: donasiHandler, transitionType: TransitionType.fadeIn);

    // Define Routing Home Beasiswa
    router.define(infoBeasiswa,
        handler: beasiswaHandler, transitionType: TransitionType.fadeIn);

    // Define Routing Home Loker
    router.define(infoLoker,
        handler: lokerHandler, transitionType: TransitionType.fadeIn);

    // Define Routing Home Give Away
    router.define(giveAway,
        handler: giveawayHandler, transitionType: TransitionType.fadeIn);

    // Define Routing Home Market
    router.define(market,
        handler: marketHandler, transitionType: TransitionType.fadeIn);
  }
}
