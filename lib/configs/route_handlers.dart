import 'package:adekuflutter/components/auth/lost_password_component.dart';
import 'package:adekuflutter/components/auth/root_component.dart';
import 'package:adekuflutter/components/auth/sign_in_component.dart';
import 'package:adekuflutter/components/auth/sign_up_component.dart';
import 'package:adekuflutter/components/home/aspirasi/aspirasi_component.dart';
import 'package:adekuflutter/components/home/aspirasi/aspirasi_success_component.dart';
import 'package:adekuflutter/components/home/beasiswa/beasiswa_component.dart';
import 'package:adekuflutter/components/home/berita/berita_component.dart';
import 'package:adekuflutter/components/home/berita/berita_detail_component.dart';
import 'package:adekuflutter/components/home/bpjs/bpjs_new_component.dart';
import 'package:adekuflutter/components/home/donasi/donasi_component.dart';
import 'package:adekuflutter/components/home/giveaway/giveaway_component.dart';
import 'package:adekuflutter/components/home/home_component.dart';
import 'package:adekuflutter/components/home/internet/internet_new_component.dart';
import 'package:adekuflutter/components/home/listrik/listrik_new_component.dart';
import 'package:adekuflutter/components/home/loker/loker_component.dart';
import 'package:adekuflutter/components/home/market/market_component.dart';
import 'package:adekuflutter/components/home/payment_component.dart';
import 'package:adekuflutter/components/home/pin_component.dart';
import 'package:adekuflutter/components/home/pulsa/pulsa_new_component.dart';
import 'package:adekuflutter/components/home/topUp/top_up_component.dart';
import 'package:adekuflutter/components/home/topUp/top_up_history_component.dart';
import 'package:adekuflutter/components/home/topUp/top_up_new_component.dart';

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';

// Auth
var rootHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return RootComponent();
});
var signInHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return SignInComponent();
});
var signUpHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return SignUpComponent();
});
var lostPasswordHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return LostPasswordComponent();
});

// PIN
var pinHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return PinComponent();
});

// PAYMENT
var paymentHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return PaymentComponent();
});

// Home
var homeHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return HomeComponent();
});

// Top Up
var topUpHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return TopUpComponent();
});
var topUpHistoryHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return TopUpHistoryComponent();
});
var topUpNewHandler =
    Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return TopUpNewComponent(title: params['new'][0]);
});

// Pulsa
var pulsaNewHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return PulsaNewComponent();
});

// Internet
var internetNewHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return InternetNewComponent();
});

// Listrik
var listrikNewHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return ListrikNewComponent();
});

// BPJS
var bpjsNewHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return BPJSNewComponent();
});

// Berita
var beritaHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return BeritaComponent();
});

var beritaDetailHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return BeritaDetailComponent();
});

// Aspirasi
var aspirasiHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return AspirasiComponent();
});
var aspirasiSuccessHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return AspirasiSuccessComponent();
});

// Donasi
var donasiHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return DonasiComponent();
});

// Beasiswa
var beasiswaHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return BeasiswaComponent();
});

// Loker
var lokerHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return LokerComponent();
});

// Giveaway
var giveawayHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return GiveAwayComponent();
});

// Market
var marketHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return MarketComponent();
});
